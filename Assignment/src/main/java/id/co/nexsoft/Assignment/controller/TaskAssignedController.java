package id.co.nexsoft.Assignment.controller;

import id.co.nexsoft.Assignment.dto.request.TaskAssigneeDTO;
import id.co.nexsoft.Assignment.dto.request.TaskAssigneeRequest;
import id.co.nexsoft.Assignment.services.TaskAssignedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/taskAssignee")
public class TaskAssignedController {

    @Autowired
    private TaskAssignedService service;

    @GetMapping("/admin/findAll")
    public ResponseEntity<?> getAllTaskAssigned() {
        return service.getAllTaskAssigneed();
    }

    @GetMapping("/admin/findAll/{approval}")
    public ResponseEntity<?> getAllTaskAssignedByApproval(@PathVariable String approval) {
        return service.getAllTaskAssigneedByApproval(approval);
    }

    @GetMapping("/admin/findAllSchedule")
    public ResponseEntity<?> getAllTaskSchedule() {
        return service.getAllSchedule();
    }

    @GetMapping("/findAllSchedule/{idUser}")
    public ResponseEntity<?> getTaskScheduleByUser(@PathVariable("idUser") int idUser) {
        return service.getScheduleByUser(idUser);
    }

    @GetMapping("/admin/findTaskReport")
    public ResponseEntity<?> getTaskReport(@RequestParam(defaultValue = "0") int pageNo,
                                           @RequestParam(defaultValue = "5") int pageSize,
                                           @RequestParam(defaultValue = "") String keywordTaskName,
                                           @RequestParam(defaultValue = "") String keywordPIC,
                                           @RequestParam(defaultValue = "") String keywordAssignee,
                                           @RequestParam(defaultValue = "") String keywordRoom) {
        return service.getTaskReport(pageNo, pageSize, keywordTaskName, keywordPIC, keywordAssignee,
                keywordRoom);
    }

    @GetMapping("/admin/getTaskReportToCSV")
    public ResponseEntity<?> getTaskReportToCSV( @RequestParam(defaultValue = "") String keywordTaskName,
                                                 @RequestParam(defaultValue = "") String keywordPIC,
                                                 @RequestParam(defaultValue = "") String keywordAssignee,
                                                 @RequestParam(defaultValue = "") String keywordRoom) {
        return service.getTaskReportToCSV(keywordTaskName, keywordPIC, keywordAssignee, keywordRoom);
    }

    @GetMapping("/findAllTask")
    public ResponseEntity<?> getAllTaskAssignedByToday(@RequestParam(defaultValue = "0") int pageNo,
                                                       @RequestParam(defaultValue = "5") int pageSize,
                                                       @RequestParam(defaultValue = "") String keyword) {
        return service.getAllTaskAssigneedByToday(pageNo, pageSize, keyword );
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<?> getTaskAssignedById(@PathVariable int id) {
        return service.getTaskAssigneedById(id);
    }

    @GetMapping("/admin/findAllPagination/{approval}")
    public ResponseEntity<?> getAllTaskAssignedPagination(@PathVariable("approval") String approval,
                                                          @RequestParam(defaultValue = "0") int pageNo,
                                                          @RequestParam(defaultValue = "5") int pageSize,
                                                          @RequestParam(defaultValue = "") String keyword) {
        return service.getTaskAssignedPagination(pageNo, pageSize, keyword, approval);
    }

    @GetMapping("/admin/findAllApproval")
    public ResponseEntity<?> getAllTaskApprovalPagination(@RequestParam(defaultValue = "0") int pageNo,
                                                          @RequestParam(defaultValue = "5") int pageSize,
                                                          @RequestParam(defaultValue = "") String keyword) {
        return service.getTaskApprovalPagination(pageNo, pageSize, keyword);
    }

    @GetMapping("/user/findAllRequest/{userId}")
    public ResponseEntity<?> getAllTaskRequestByUser(@PathVariable("userId") int userId,
                                                     @RequestParam(defaultValue = "0") int pageNo,
                                                     @RequestParam(defaultValue = "5") int pageSize,
                                                     @RequestParam(defaultValue = "") String keyword) {
        return service.getAllTaskRequestByUser(pageNo, pageSize, userId, keyword);
    }

    @GetMapping("/user/findAllHistory/{userId}")
    public ResponseEntity<?> getAllTaskHistoryByUser(@PathVariable("userId") int userId,
                                                     @RequestParam(defaultValue = "0") int pageNo,
                                                     @RequestParam(defaultValue = "5") int pageSize,
                                                     @RequestParam(defaultValue = "") String keyword) {
        return service.getAllTaskHistoryByUser(pageNo, pageSize, userId, keyword);
    }

    @PostMapping("/admin/createTaskAssigned")
    public ResponseEntity<?> createTaskAssigned(@Valid @RequestBody TaskAssigneeDTO taskAssigneeDTO) {
        return service.createTaskAssigneed(taskAssigneeDTO);
    }

    @PutMapping("/admin/updateTaskAssigned/{id}")
    public ResponseEntity<?> updateTaskAssigned(@PathVariable int id,
                                                @Valid @RequestBody TaskAssigneeDTO taskAssigneeDTO) {
        return service.updateTaskAssignee(id, taskAssigneeDTO);
    }

    @PutMapping("/user/updateTaskAssigned/{id}")
    public ResponseEntity<?> updateTaskAssignedByUser(@PathVariable int id,
                                                      @Valid @RequestBody TaskAssigneeRequest request) {
        return service.updateTaskAssignedByUser(id, request);
    }

    @PutMapping("/admin/approveTaskAssigned/{idTask}/{approval}")
    public ResponseEntity<?> approveTaskAssigned(@PathVariable("idTask") int idTask,
                                                 @PathVariable("approval") String approval) {
        return service.approvalTaskAssigned(idTask, approval);
    }

    @PutMapping("/user/assignEmployee/{idTask}/{idEmployee}")
    public ResponseEntity<?> assignEmployee(@PathVariable("idTask") int idTask,
                                            @PathVariable("idEmployee") int idEmployee) {
        return service.assignEmployee(idTask, idEmployee);
    }

    @PutMapping("/admin/deleteTaskAssigned/{id}")
    public ResponseEntity<?> deleteTaskAssigned(@PathVariable int id) {
        return service.deleteTaskAssigned(id);
    }
}
