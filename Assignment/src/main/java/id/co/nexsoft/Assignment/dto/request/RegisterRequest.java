package id.co.nexsoft.Assignment.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RegisterRequest {
    @NotBlank
    @NotNull
    private String name;

    @NotBlank
    @NotNull
    @Email
    private String email;

    @NotBlank
    @NotNull
    private String username;

    @NotBlank
    @NotNull
    @Size(min = 6, max = 8)
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,8}$",
            message = "min 6, max 8, 1 uppercase, 1 lowercase, 1 number")
    private String password;

    @NotBlank
    @NotNull
    private String position;
}
