package id.co.nexsoft.Assignment.services;

import id.co.nexsoft.Assignment.WebSecurityConfig;
import id.co.nexsoft.Assignment.dto.request.LoginRequest;
import id.co.nexsoft.Assignment.dto.request.RegisterRequest;
import id.co.nexsoft.Assignment.dto.response.EmployeeDTO;
import id.co.nexsoft.Assignment.dto.response.LoginResponse;
import id.co.nexsoft.Assignment.entities.Employee;
import id.co.nexsoft.Assignment.jwt.JwtUtil;
import id.co.nexsoft.Assignment.repository.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

@Service
public class EmployeeService {
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;
    private final MyUserDetailsService userDetailsService;

    @Autowired
    private EmployeeRepository repo;

    @Autowired
    private WebSecurityConfig passwordEncoder;

    @Autowired
    private EmailService emailService;

    private ModelMapper modelMapper = new ModelMapper();

    public EmployeeService(AuthenticationManager authenticationManager, JwtUtil jwtUtil,
                           MyUserDetailsService userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.userDetailsService = userDetailsService;
    }

    //Find All Employee
    public ResponseEntity<?> findAllEmployee() {
        return ResponseEntity.ok(repo.findAll());
    }

    public ResponseEntity<?> getAllEmployeeNoCondition() {
        return ResponseEntity.ok(repo.findAllNoCondition());
    }

    public ResponseEntity<?> findAllEmployeeLeader() {
        return ResponseEntity.ok(repo.findEmployeeLeader());
    }

    public ResponseEntity<?> findAllApproved() {
        return ResponseEntity.ok(repo.findAllisApproved());
    }

    //FindByIdDTO
    public ResponseEntity<?> findEmployeeByIdDTO(int id) {
        EmployeeDTO employeeDTO = repo.findByIdDTO(id);

        if (employeeDTO == null) {
            return ResponseEntity.badRequest().body("Employee not found!");
        }

        return ResponseEntity.ok(employeeDTO);
    }

    //Pagination User By isApprove
    public ResponseEntity<?> getEmployeeByIsApprovePagination(int pageNo, int pageSize, String keyword,
                                                              String approval) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<EmployeeDTO> employee = repo.employeeIsApproveSearch(keyword, approval, pageable);
        return ResponseEntity.ok(employee);
    }

    //Register Employee
    public ResponseEntity<?> registerEmployeeByUser(RegisterRequest registerRequest) {
        if (repo.existsByUsername(registerRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body("Username already taken!");
        }
        if (repo.existsByEmail((registerRequest.getEmail()))) {
            return ResponseEntity
                    .badRequest()
                    .body("Email already in use!");
        }

            Employee employee = modelMapper.map(registerRequest, Employee.class);
            employee.setPassword(passwordEncoder.passwordEncoder().encode(registerRequest.getPassword()));
            employee.setRole("ROLE_USER");
            employee.setIsApprove("Pending");
            employee.setCreateAt(LocalDate.now());
            repo.save(employee);
            return ResponseEntity.ok("Register success! Please wait confirmation from admin!");
    }

    //Register User By Admin
    public ResponseEntity<?> registerUserByAdmin(RegisterRequest registerRequest) {
        if (repo.existsByUsername(registerRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body("Username already taken!");
        }
        if (repo.existsByEmail((registerRequest.getEmail()))) {
            return ResponseEntity
                    .badRequest()
                    .body("Email already in use!");
        }

        Employee employee = modelMapper.map(registerRequest, Employee.class);
        employee.setPassword(passwordEncoder.passwordEncoder().encode(registerRequest.getPassword()));
        employee.setRole("ROLE_USER");
        employee.setIsApprove("Approved");
        employee.setCreateAt(LocalDate.now());
        repo.save(employee);
        return ResponseEntity.ok("Success create employee with username : " + registerRequest.getUsername());
    }

    //Edit Employee by User
    public ResponseEntity<?> updateEmployeeByUser(EmployeeDTO employeeDTO, int idUser) {
        Employee employee = repo.findById(idUser);

        if (employee == null) {
            return ResponseEntity.badRequest().body("User not found!");
        }

        if (repo.checkExistsUsernameUpdate(idUser, employeeDTO.getUsername()) == 1) {
            return ResponseEntity.badRequest().body("Username already taken!");
        }

        if (repo.checkExistsEmailUpdate(idUser, employeeDTO.getEmail()) == 1) {
            return ResponseEntity.badRequest().body("Email already taken!");
        }

        employee.setUsername(employeeDTO.getUsername().trim());
        employee.setName(employeeDTO.getName().trim());
        employee.setEmail(employeeDTO.getEmail().trim());
        repo.save(employee);
        EmployeeDTO response = modelMapper.map(employee, EmployeeDTO.class);
        return ResponseEntity.ok(response);
    }

    //Edit employee by admin
    public ResponseEntity<?> updateEmployeeByAdmin(EmployeeDTO employeeDTO, int idUser) {
        Employee employee = repo.findById(idUser);

        if (employee == null) {
            return ResponseEntity.badRequest().body("User not found!");
        }

        if (repo.checkExistsUsernameUpdate(idUser, employeeDTO.getUsername()) == 1) {
            return ResponseEntity.badRequest().body("Username already taken!");
        }

        if (repo.checkExistsEmailUpdate(idUser, employeeDTO.getEmail()) == 1) {
            return ResponseEntity.badRequest().body("Email already taken!");
        }

        employee.setUsername(employeeDTO.getUsername().trim());
        employee.setName(employeeDTO.getName().trim());
        employee.setEmail(employeeDTO.getEmail().trim());
        employee.setPosition(employeeDTO.getPosition());
        repo.save(employee);
        return ResponseEntity.ok("Success update data user with ID : " + idUser);
    }

    //Approval user by admin
    public ResponseEntity<?> approvalEmployee(int idEmployee, String approval) {
        Employee employee = repo.findById(idEmployee);
        String to = "";
        String topic = "Approval User";
        String body = "";

        if (employee == null) {
            return ResponseEntity.badRequest().body("User not found!");
        }

        if ("Approved".equalsIgnoreCase(employee.getIsApprove())) {
            return ResponseEntity.badRequest().body("User already approved!");
        }

        to = employee.getEmail();
        if ("Approved".equalsIgnoreCase(approval)) {
            body = "== APPROVAL TASK ASSIGNEE == \n" +
                    "Your request for join us by \n" +
                    "Name : " + employee.getName() + "\n" +
                    "Username : " + employee.getUsername() + "\n" +
                    "Position : " + employee.getPosition() + "\n" +
                    "is Approved. Please prepare yourself well. Congrats adn Thank you for join us.";
            employee.setIsApprove("Approved");
            repo.save(employee);
            emailService.sendEmail(to, body, topic);
            return ResponseEntity.ok("Success Approve User with ID : " + idEmployee);
        } else if ("Rejected".equalsIgnoreCase(approval)) {
            body = "== APPROVAL TASK ASSIGNEE == \n" +
                    "Your request for join us by \n" +
                    "Name : " + employee.getName() + "\n" +
                    "Username : " + employee.getUsername() + "\n" +
                    "Position : " + employee.getPosition() + "\n" +
                    "is Rejected. Maybe you can register next time. Forgive us and thank you for trying " +
                    "join us.";
            repo.delete(employee);
            emailService.sendEmail(to, body, topic);
            return ResponseEntity.ok("Success Reject & Delete user with ID : " + idEmployee);
        } else {
            return ResponseEntity.badRequest().body("Bad Request! Option Not Found!");
        }
    }

    //Login
    public ResponseEntity<?> createAuth(LoginRequest loginRequest) throws Exception {
        Employee employee = repo.findByUsername(loginRequest.getUsername());

        try {
            if (!repo.existsByUsername(loginRequest.getUsername())) {
                return ResponseEntity
                        .badRequest()
                        .body("Incorrect username or password");
            }

            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken
                            (loginRequest.getUsername(), loginRequest.getPassword()));

            if (!"Approved".equalsIgnoreCase(employee.getIsApprove())) {
                return ResponseEntity
                        .badRequest()
                        .body("User is still not approved. Please contact admin first!");
            }

            UserDetails userDetails = userDetailsService.loadUserByUsername(loginRequest.getUsername());

            final String jwt = jwtUtil.generateToken(userDetails);

            EmployeeDTO emp = modelMapper.map(employee, EmployeeDTO.class);
            LoginResponse res = new LoginResponse();
            res.setUser(emp);
            res.setToken(jwt);
            res.setRole(userDetails.getAuthorities().toArray()[0].toString());
            return ResponseEntity.ok(res);

        } catch (BadCredentialsException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Incorrect username or password");
        }
    }

    //Delete User
    public ResponseEntity<?> deleteEmployee(int idUser) {
        Employee employee = repo.findById(idUser);

        if (employee == null) {
            return ResponseEntity.badRequest().body("User not found!");
        }

        if ("Pending".equalsIgnoreCase(employee.getIsApprove())) {
            return ResponseEntity.badRequest().body("Can't delete user because it hasn't been approved!");
        }

        employee.setDeleteAt(LocalDate.now());
        repo.save(employee);
        return ResponseEntity.ok("Success delete user with ID : " + idUser);
    }
}
