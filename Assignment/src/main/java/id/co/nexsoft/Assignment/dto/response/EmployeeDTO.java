package id.co.nexsoft.Assignment.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EmployeeDTO {
    private int id;

    @NotNull
    @Size(max = 75)
    private String name;

    @NotNull
    @Size(max = 75)
    @Email
    private String email;

    @NotNull
    @Size(max = 30)
    private String username;

    private String position;

    private String avatar;
}
