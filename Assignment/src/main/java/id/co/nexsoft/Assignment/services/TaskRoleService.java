package id.co.nexsoft.Assignment.services;

import id.co.nexsoft.Assignment.entities.TaskRole;
import id.co.nexsoft.Assignment.repository.TaskRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class TaskRoleService {

    @Autowired
    private TaskRoleRepository repo;

    public ResponseEntity<?> getAllTaskRoleNoCondition() {
        return ResponseEntity.ok(repo.findAllNoCondition());
    }

    public ResponseEntity<?> getAllTaskRole() {
        return ResponseEntity.ok(repo.findAll());
    }

    public ResponseEntity<?> getTaskRolePagination(int pageNo, int pageSize, String keyword) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<TaskRole> taskRole = repo.roleSearch(keyword, pageable);
        return ResponseEntity.ok(taskRole);
    }

    public ResponseEntity<?> getTaskRoleById(int id) {
        TaskRole taskRole = repo.findById(id);

        if (taskRole == null) {
            return ResponseEntity.badRequest().body("Task role not found!");
        }

        return ResponseEntity.ok(taskRole);
    }

    public ResponseEntity<?> createTaskRole(TaskRole taskRole) {
        TaskRole data = new TaskRole();
        data.setTaskRole(taskRole.getTaskRole().trim());
        return ResponseEntity.ok(repo.save(data));
    }

    public ResponseEntity<?> updateTaskRole(int id, TaskRole newData) {
        TaskRole data = repo.findById(id);

        if (data == null) {
            return ResponseEntity.badRequest().body("Task role Not Found!");
        }

        data.setTaskRole(newData.getTaskRole().trim());
        return ResponseEntity.ok(repo.save(data));
    }

    public ResponseEntity<?> deleteTaskRole(int id) {
        TaskRole data = repo.findById(id);

        if (data == null) {
            return ResponseEntity.badRequest().body("Failed Delete Task role!");
        }

        data.setDeleteAt(LocalDate.now());
        repo.save(data);
        return ResponseEntity.ok("Success delete Task Role with ID: " + id);
    }
}
