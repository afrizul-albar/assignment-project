package id.co.nexsoft.Assignment.controller;

import id.co.nexsoft.Assignment.entities.TaskRole;
import id.co.nexsoft.Assignment.services.TaskRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/role")
public class TaskRoleController {

    @Autowired
    private TaskRoleService service;

    @GetMapping("/findAllNoCodition")
    public ResponseEntity<?> getAllTaskRoleNoCondition() {
        return service.getAllTaskRoleNoCondition();
    }

    @GetMapping("/findAll")
    public ResponseEntity<?> getAllTaskRole() {
        return service.getAllTaskRole();
    }

    @GetMapping("/findAllPagination")
    public ResponseEntity<?> getAllTaskRoleController(@RequestParam(defaultValue = "0") int pageNo,
                                                      @RequestParam(defaultValue = "5") int pageSize,
                                                      @RequestParam(defaultValue = "") String keyword) {
        return service.getTaskRolePagination(pageNo, pageSize, keyword);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<?> getTaskRoleById(@PathVariable int id) {
        return service.getTaskRoleById(id);
    }

    @PostMapping("/createTaskRole")
    public ResponseEntity<?> createTaskRole(@Valid @RequestBody TaskRole taskRole) {
        return service.createTaskRole(taskRole);
    }

    @PutMapping("/updateTaskRole/{id}")
    public ResponseEntity<?> updateTaskRole(@PathVariable int id, @Valid @RequestBody TaskRole taskRole) {
        return service.updateTaskRole(id, taskRole);
    }

    @PutMapping("/deleteTaskRole/{id}")
    public ResponseEntity<?> deleteTaskRole(@PathVariable int id) {
        return service.deleteTaskRole(id);
    }
}
