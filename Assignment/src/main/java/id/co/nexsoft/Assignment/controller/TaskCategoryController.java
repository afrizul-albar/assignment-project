package id.co.nexsoft.Assignment.controller;

import id.co.nexsoft.Assignment.entities.TaskCategory;
import id.co.nexsoft.Assignment.services.TaskCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/category")
public class TaskCategoryController {

    @Autowired
    private TaskCategoryService service;

    @GetMapping("/findAllNoCondition")
    public ResponseEntity<?> getAllTaskCtegoryNoCondition() {
        return service.getAllTaskCategoryNoCondition();
    }

    @GetMapping("/findAll")
    public ResponseEntity<?> getAllTaskCategory() {
        return service.getAllTaskCategory();
    }

    @GetMapping("/findAllPagination")
    public ResponseEntity<?> getAllTaskCategoryPagination(@RequestParam(defaultValue = "0") int pageNo,
                                                          @RequestParam(defaultValue = "5") int pageSize,
                                                          @RequestParam(defaultValue = "") String keyword) {
        return service.getTaskCategoryPagination(pageNo, pageSize, keyword);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<?> getTaskCategoryById(@PathVariable int id) {
        return service.getTaskCategoryById(id);
    }

    @PostMapping("/createTaskCategory")
    public ResponseEntity<?> createTaskCategory(@Valid @RequestBody TaskCategory taskCategory) {
        return service.createTaskCategory(taskCategory);
    }

    @PutMapping("/updateTaskCategory/{id}")
    public ResponseEntity<?> updateTaskCategory(@PathVariable int id,
                             @Valid @RequestBody TaskCategory taskCategory) {
        return service.updateTaskCategory(id, taskCategory);
    }

    @PutMapping("/deleteTaskCategory/{id}")
    public ResponseEntity<?> deleteTaskCategory(@PathVariable int id) {
        return service.deleteTaskCategory(id);
    }
}
