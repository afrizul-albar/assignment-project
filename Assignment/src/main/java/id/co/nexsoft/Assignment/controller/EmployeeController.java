package id.co.nexsoft.Assignment.controller;

import id.co.nexsoft.Assignment.dto.request.LoginRequest;
import id.co.nexsoft.Assignment.dto.request.RegisterRequest;
import id.co.nexsoft.Assignment.dto.response.EmployeeDTO;
import id.co.nexsoft.Assignment.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RequestMapping("api/employee")
@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService service;

    @GetMapping("/findAll")
    public ResponseEntity<?> findAllEmployee() {
        return service.findAllEmployee();
    }

    @GetMapping("/admin/findAllNoCondition")
    public ResponseEntity<?> getAllEmployeeNoCondition() {
        return service.getAllEmployeeNoCondition();
    }

    @GetMapping("/admin/findAllLeader")
    public ResponseEntity<?> findAllEmployeeReader() {
        return service.findAllEmployeeLeader();
    }

    @GetMapping("/admin/findAllApproved")
    public ResponseEntity<?> findAllApproved() {
        return service.findAllApproved();
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<?> findEmployeeById(@PathVariable("id") int id) {
        return service.findEmployeeByIdDTO(id);
    }

    @GetMapping("/admin/findAll/{approval}")
    public ResponseEntity<?> findAllEmployeeByApproval(@PathVariable("approval") String approval,
                                                       @RequestParam(defaultValue = "0") int pageNo,
                                                       @RequestParam(defaultValue = "5") int pageSize,
                                                       @RequestParam(defaultValue = "") String keyword) {
        return service.getEmployeeByIsApprovePagination(pageNo, pageSize, keyword, approval);
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerEmployeeByUser(@Valid @RequestBody RegisterRequest registerRequest) {
        return service.registerEmployeeByUser(registerRequest);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginRequest loginRequest) throws Exception {
        return service.createAuth(loginRequest);
    }

    @PostMapping("/admin/register")
    public ResponseEntity<?> registerUserByAdmin(@Valid @RequestBody RegisterRequest registerRequest) {
        return service.registerUserByAdmin(registerRequest);
    }

    @PutMapping("/admin/updateEmployee/{idUser}")
    public ResponseEntity<?> updateEmployeeByAdmin(@PathVariable("idUser") int idUser,
                                                   @Valid @RequestBody EmployeeDTO employeeDTO) {
        return service.updateEmployeeByAdmin(employeeDTO, idUser);
    }

    @PutMapping("/updateEmployee/{idUser}")
    public ResponseEntity<?> updateEmployeeByUser(@PathVariable("idUser") int idUser,
                                                  @Valid @RequestBody EmployeeDTO employeeDTO) {
        return service.updateEmployeeByUser(employeeDTO, idUser);
    }

    @PutMapping("/admin/approvalEmployee/{idUser}/{approval}")
    public ResponseEntity<?> approvalEmployee(@PathVariable("idUser") int idUser,
                                              @PathVariable("approval") String approval) {
        return service.approvalEmployee(idUser, approval);
    }

    @PutMapping("/admin/deleteEmployee/{idUser}")
    public ResponseEntity<?> deleteEmployee(@PathVariable("idUser") int idUser) {
        return service.deleteEmployee(idUser);
    }
}
