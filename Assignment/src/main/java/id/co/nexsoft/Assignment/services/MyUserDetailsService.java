package id.co.nexsoft.Assignment.services;

import id.co.nexsoft.Assignment.dto.security.MyUserDetails;
import id.co.nexsoft.Assignment.entities.Employee;
import id.co.nexsoft.Assignment.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private EmployeeRepository repo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Employee> employee =Optional.ofNullable(repo.findByUsername(username));
        return employee.map(MyUserDetails::new).get();
    }
}
