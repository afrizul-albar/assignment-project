package id.co.nexsoft.Assignment.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TaskAssigneeRequest {

    private LocalDate date;

    private LocalTime startTime;

    private LocalTime endTime;

    private int processId;
}
