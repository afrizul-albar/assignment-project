package id.co.nexsoft.Assignment.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TaskList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String taskName;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate endDate;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_room")
    private Room room;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_taskCategory")
    private TaskCategory taskCategory;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_PIC")
    private Employee pic;

    private LocalDate deleteAt;
}
