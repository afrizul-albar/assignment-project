package id.co.nexsoft.Assignment.services;

import id.co.nexsoft.Assignment.dto.request.TaskListDTO;
import id.co.nexsoft.Assignment.entities.Employee;
import id.co.nexsoft.Assignment.entities.Room;
import id.co.nexsoft.Assignment.entities.TaskCategory;
import id.co.nexsoft.Assignment.entities.TaskList;
import id.co.nexsoft.Assignment.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class TaskListService {

    @Autowired
    private TaskListRepository taskListRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private TaskCategoryRepository categoryRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TaskAssignedRepository taskAssignedRepository;

    public ResponseEntity<?> getAllTaskList() {
        return ResponseEntity.ok(taskListRepository.findAll());
    }

    public ResponseEntity<?> getAllTaskListNoCondition() {
        return ResponseEntity.ok(taskListRepository.findAllNoCondition());
    }

    public ResponseEntity<?> getTaskListForForm() {
        return ResponseEntity.ok(taskListRepository.findTaskListForm());
    }

    public ResponseEntity<?> getTaskListPagination(int pageNo, int pageSize, String keyword) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("startDate").descending());
        Page<TaskList> taskLists = taskListRepository.taskListSearch(keyword, pageable);
        return ResponseEntity.ok(taskLists);
    }


    public ResponseEntity<?> getTaskListById(int id) {
        TaskList taskList = taskListRepository.findById(id);

        if (taskList == null) {
            return ResponseEntity.badRequest().body("Task Not Found!");
        }

        return ResponseEntity.ok(taskList);
    }

    public ResponseEntity<?> createTaskList(TaskListDTO taskListDTO) {
        Room dataRoom = roomRepository.findById(taskListDTO.getRoomId());
        TaskCategory dataCategory = categoryRepository.findById(taskListDTO.getCategoryId());
        Employee dataPIC = employeeRepository.findById(taskListDTO.getPicId());

        if (dataRoom == null || dataCategory == null || dataPIC == null) {
            return ResponseEntity.badRequest().body("Bad Request!");
        }

        if (taskListDTO.getStartDate().isBefore(LocalDate.now()) ||
                taskListDTO.getEndDate().isBefore(LocalDate.now())) {
            return ResponseEntity.badRequest().body("Start Date and End Date can't be less than today!");
        }

        if (taskListDTO.getStartDate().isAfter(taskListDTO.getEndDate()) ||
                taskListDTO.getEndDate().isBefore(taskListDTO.getStartDate())) {
            String message = taskListDTO.getStartDate().isAfter(taskListDTO.getEndDate())
                    ? "Start Date can't be more than end date!"
                    : "End Date can't be less than start date!";
            return ResponseEntity.badRequest().body(message);
        }

        if (dataPIC.getIsApprove().equalsIgnoreCase("Pending")) {
            return ResponseEntity.badRequest().body("User has not been approved! Choose another user!");
        }

        if (!"Leader".equalsIgnoreCase(dataPIC.getPosition()) ||
                dataPIC.getRole().equalsIgnoreCase("ROLE_ADMIN")) {
            String message = !"Leader".equalsIgnoreCase(dataPIC.getPosition())
                    ? "Only leader can be PIC!"
                    : "Admin can't be PIC";
            return ResponseEntity.badRequest().body(message);
        }

        if (taskListRepository.checkRoomConflict(taskListDTO.getRoomId(), taskListDTO.getStartDate(),
                taskListDTO.getEndDate()) == 1) {
            return ResponseEntity.badRequest().body("Room " + dataRoom.getRoom() + " is used on " +
                    "that date. Please choose another Room!");
        }

        TaskList dataTask = new TaskList();
        dataTask.setTaskName(taskListDTO.getTaskName().trim());
        dataTask.setStartDate(taskListDTO.getStartDate());
        dataTask.setEndDate(taskListDTO.getEndDate());
        dataTask.setRoom(dataRoom);
        dataTask.setTaskCategory(dataCategory);
        dataTask.setPic(dataPIC);
        taskListRepository.save(dataTask);
        return ResponseEntity.ok("Success create task with Task Name : " + dataTask.getTaskName());
    }

    public ResponseEntity<?> updateTaskList(int id, TaskListDTO newData) {
        TaskList dataTask = taskListRepository.findById(id);
        Room dataRoom = roomRepository.findById(newData.getRoomId());
        TaskCategory dataCategory = categoryRepository.findById(newData.getCategoryId());
        Employee dataPIC = employeeRepository.findById(newData.getPicId());

        if (dataTask == null || dataRoom == null || dataCategory == null || dataPIC == null) {
            return ResponseEntity.badRequest().body("Failed create Task!");
        }

        if (dataTask.getEndDate().isBefore(LocalDate.now())) {
            return ResponseEntity.badRequest().body("Task List is expired!");
        }

        if (newData.getEndDate().isBefore(LocalDate.now())) {
            return ResponseEntity.badRequest().body("End Date can't be less than today!");
        }

        if (newData.getStartDate().isAfter(newData.getEndDate()) ||
                newData.getEndDate().isBefore(newData.getStartDate())) {
            String message = newData.getStartDate().isAfter(newData.getEndDate())
                    ? "Start Date can't be more than end date!"
                    : "End Date can't be less than start date!";
            return ResponseEntity.badRequest().body(message);
        }

        if (taskListRepository.checkRoomConflictUpdate(id, newData.getRoomId(), newData.getStartDate(),
                newData.getEndDate()) == 1) {
            return ResponseEntity.badRequest().body("Room  " + dataRoom.getRoom() + "is used on " +
                    "that date. Please choose another Room!");
        }

        if (dataPIC.getIsApprove().equalsIgnoreCase("Pending")) {
            return ResponseEntity.badRequest().body("User has not been approved! Choose another user!");
        }

        if (!"Leader".equalsIgnoreCase(dataPIC.getPosition()) ||
            dataPIC.getRole().equalsIgnoreCase("ROLE_ADMIN")) {
            String message = !"Leader".equalsIgnoreCase(dataPIC.getPosition())
                    ? "Only leader can be PIC!"
                    : "Admin can't be PIC";
            return ResponseEntity.badRequest().body(message);
        }

        dataTask.setTaskName(newData.getTaskName());
        if (taskAssignedRepository.checkByTaskExists(id) == 0) {
            dataTask.setStartDate(newData.getStartDate());
        }
        dataTask.setEndDate(newData.getEndDate());
        dataTask.setRoom(dataRoom);
        dataTask.setTaskCategory(dataCategory);
        dataTask.setPic(dataPIC);
        taskListRepository.save(dataTask);
        return ResponseEntity.ok("Success update task with Task Name : " + dataTask.getTaskName());
    }

    public ResponseEntity<?> deleteTaskList(int id) {
        TaskList data = taskListRepository.findById(id);

        if (data == null) {
            return ResponseEntity.badRequest().body("Task not found!");
        }

        if (taskAssignedRepository.checkByTaskExists(id) == 1) {
            return ResponseEntity.badRequest().body("Task can't be deleted!");
        }

        data.setDeleteAt(LocalDate.now());
        taskListRepository.save(data);
        return ResponseEntity.ok("Success delete Task with ID: " + id);
    }

}
