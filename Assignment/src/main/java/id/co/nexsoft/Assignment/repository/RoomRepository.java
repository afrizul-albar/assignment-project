package id.co.nexsoft.Assignment.repository;

import id.co.nexsoft.Assignment.entities.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepository extends CrudRepository<Room, Integer> {

    @Query(value = "SELECT r.id, r.room, r.delete_at from room r", nativeQuery = true)
    List<Room> findAllNoCondition();

    @Query(value = "SELECT r.id, r.room, r.delete_at " +
            "from room r WHERE r.delete_at is null AND r.id = :id", nativeQuery = true)
    Room findById(@Param("id") int id);

    @Query(value = "SELECT r.id, r.room, r.delete_at from room r WHERE r.delete_at is null", nativeQuery = true)
    List<Room> findAll();

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.Room (r.id, r.room, r.deleteAt) from " +
            "Room r WHERE r.deleteAt is null AND r.room LIKE %:keyword%")
    Page<Room> roomSearch(@Param("keyword") String keyword, Pageable pageable);
}
