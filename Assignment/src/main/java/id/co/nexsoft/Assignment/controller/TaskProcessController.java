package id.co.nexsoft.Assignment.controller;

import id.co.nexsoft.Assignment.entities.TaskProcess;
import id.co.nexsoft.Assignment.services.TaskProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/process")
public class TaskProcessController {

    @Autowired
    private TaskProcessService service;

    @GetMapping("/findAll")
    public ResponseEntity<?> getAllTaskProcess() {
        return service.getALlTaskProcess();
    }

    @GetMapping("/admin/findById/{id}")
    public ResponseEntity<?> getTaskProcessById(@PathVariable int id) {
        return service.getTaskProcessById(id);
    }

    @GetMapping("/admin/findAllPagination")
    public ResponseEntity<?> getAllTaskProcessPagination(@RequestParam(defaultValue = "0") int pageNo,
                                                         @RequestParam(defaultValue = "5") int pageSize,
                                                         @RequestParam(defaultValue = "") String keyword) {
        return service.getProcessPagination(pageNo, pageSize, keyword);
    }

    @PostMapping("/admin/createTaskProcess")
    public ResponseEntity<?> createTaskProcess(@Valid @RequestBody TaskProcess taskProcess) {
        return service.createTaskProcess(taskProcess);
    }

    @PutMapping("/admin/updateTaskProcess/{id}")
    public ResponseEntity<?> updateTaskProcess(@PathVariable int id,
                             @Valid @RequestBody TaskProcess taskProcess) {
        return service.updateTaskProcess(id, taskProcess);
    }

    @PutMapping("/admin/deleteTaskProcess/{id}")
    public ResponseEntity<?> deleteTaskProcess(@PathVariable int id) {
        return service.deleteTaskProcess(id);
    }
}
