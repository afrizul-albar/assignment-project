package id.co.nexsoft.Assignment.repository;

import id.co.nexsoft.Assignment.entities.TaskAssigned;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface TaskAssignedRepository extends CrudRepository<TaskAssigned, Integer> {
    @Query(value = "SELECT ta.id, ta.date, ta.delete_at, ta.end_time, ta.is_approve, ta.start_time, " +
            "ta.id_employee, ta.id_task, ta.id_process, ta.id_task_role from task_assigned ta LEFT JOIN " +
            "employee e ON ta.id_employee = e.id LEFT JOIN task_list tl ON ta.id_task = tl.id LEFT JOIN " +
            "task_process tp ON ta.id_process = tp.id LEFT JOIN task_role tr ON ta.id_task_role = tr.id " +
            "WHERE ta.delete_at is null AND ta.id = :id", nativeQuery = true)
    TaskAssigned findById(@Param("id") int id);

    @Query(value = "SELECT ta.id, ta.date, ta.delete_at, ta.end_time, ta.is_approve, ta.start_time, " +
            "ta.id_employee, ta.id_task, ta.id_process, ta.id_task_role from task_assigned ta LEFT JOIN " +
            "employee e ON ta.id_employee = e.id LEFT JOIN task_list tl ON ta.id_task = tl.id LEFT JOIN " +
            "task_process tp ON ta.id_process = tp.id LEFT JOIN task_role tr ON ta.id_task_role = tr.id " +
            "WHERE ta.delete_at is null", nativeQuery = true)
    List<TaskAssigned> findAll();

    @Query(value = "SELECT ta.id, ta.date, ta.delete_at, ta.end_time, ta.is_approve, ta.start_time, " +
            "ta.id_employee, ta.id_task, ta.id_process, ta.id_task_role from task_assigned ta LEFT JOIN " +
            "employee e ON ta.id_employee = e.id LEFT JOIN task_list tl ON ta.id_task = tl.id LEFT JOIN " +
            "task_process tp ON ta.id_process = tp.id LEFT JOIN task_role tr ON ta.id_task_role = tr.id " +
            "WHERE ta.delete_at is null AND ta.is_approve = :approval", nativeQuery = true)
    List<TaskAssigned> findAllByApproval(@Param("approval") String approval);

    @Query(value = "SELECT ta.id, ta.date, ta.delete_at, ta.end_time, ta.is_approve, ta.start_time, " +
            "ta.id_employee, ta.id_task, ta.id_process, ta.id_task_role from task_assigned ta LEFT JOIN " +
            "employee e ON ta.id_employee = e.id LEFT JOIN task_list tl ON ta.id_task = tl.id LEFT JOIN " +
            "task_process tp ON ta.id_process = tp.id LEFT JOIN task_role tr ON ta.id_task_role = tr.id " +
            "WHERE ta.delete_at is null AND ta.is_approve = 'Approved' ORDER BY ta.date ASC",
            nativeQuery = true)
    List<TaskAssigned> findAllSchedule();

    @Query(value = "SELECT ta.id, ta.date, ta.delete_at, ta.end_time, ta.is_approve, ta.start_time, " +
            "ta.id_employee, ta.id_task, ta.id_process, ta.id_task_role from task_assigned ta LEFT JOIN " +
            "employee e ON ta.id_employee = e.id LEFT JOIN task_list tl ON ta.id_task = tl.id LEFT JOIN " +
            "task_process tp ON ta.id_process = tp.id LEFT JOIN task_role tr ON ta.id_task_role = tr.id " +
            "WHERE ta.delete_at is null AND ta.is_approve = 'Approved' AND ta.id_employee = :idUser " +
            "ORDER BY ta.date ASC", nativeQuery = true)
    List<TaskAssigned> findScheduleByUser(@Param("idUser") int idUser);

    @Query(value = "SELECT ta.id, ta.date, ta.delete_at, ta.end_time, ta.is_approve, ta.start_time, " +
            "ta.id_employee, ta.id_task, ta.id_process, ta.id_task_role from task_assigned ta LEFT JOIN " +
            "employee e ON ta.id_employee = e.id LEFT JOIN task_list tl ON ta.id_task = tl.id LEFT JOIN " +
            "employee pic ON tl.id_pic = pic.id LEFT JOIN room r ON tl.id_room = r.id LEFT JOIN task_process " +
            "tp ON ta.id_process = tp.id LEFT JOIN task_role tr ON ta.id_task_role = tr.id " +
            "WHERE ta.delete_at is null AND ta.is_approve = 'Approved' AND tp.process = 'Complete' " +
            "AND tl.task_name LIKE %:keywordTaskName% AND pic.name LIKE %:keywordPIC% AND e.name LIKE " +
            "%:keywordAssignee% AND r.room LIKE %:keywordRoom% ORDER BY ta.date ASC", nativeQuery = true)
    List<TaskAssigned> getTaskReportToCSV(@Param("keywordTaskName") String keywordTaskName,
                                          @Param("keywordPIC") String keywordPIC,
                                          @Param("keywordAssignee") String keywordAssignee,
                                          @Param("keywordRoom") String keywordRoom);

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.TaskAssigned (ta.id, ta.task, ta.assignee, " +
            "ta.taskRole, ta.date, ta.startTime, ta.endTime, ta.taskProcess, ta.isApprove, ta.deleteAt) FROM " +
            "TaskAssigned ta LEFT JOIN ta.task tl LEFT JOIN ta.assignee e LEFT JOIN ta.taskRole tr LEFT JOIN " +
            "ta.taskProcess tp WHERE ta.deleteAt is null AND ta.isApprove = 'Approved' AND tp.process = 'Complete' " +
            "AND tl.taskName LIKE %:keywordTaskName% AND tl.pic.name LIKE %:keywordPIC% AND e.name LIKE " +
            "%:keywordAssignee% AND tl.room.room LIKE %:keywordRoom%")
    Page<TaskAssigned> getTaskReport(@Param("keywordTaskName") String keywordTaskName,
                                     @Param("keywordPIC") String keywordPIC,
                                     @Param("keywordAssignee") String keywordAssignee,
                                     @Param("keywordRoom") String keywordRoom,
                                     Pageable pageable);

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.TaskAssigned (ta.id, ta.task, ta.assignee, " +
            "ta.taskRole, ta.date, ta.startTime, ta.endTime, ta.taskProcess, ta.isApprove, ta.deleteAt) FROM " +
            "TaskAssigned ta LEFT JOIN ta.task tl LEFT JOIN ta.assignee e LEFT JOIN ta.taskRole tr LEFT JOIN " +
            "ta.taskProcess tp WHERE ta.deleteAt is null AND ta.endTime > curtime() AND ta.isApprove = 'Pending' " +
            "AND ta.date >= curdate() " +
            "AND ta.assignee is null AND (tl.taskName LIKE %:keyword% OR e.name LIKE %:keyword% OR " +
            "tr.taskRole LIKE %:keyword%)")
    Page<TaskAssigned> findTaskToAssignSearch(@Param("keyword") String keyword, Pageable pageable);

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.TaskAssigned (ta.id, ta.task, ta.assignee, " +
            "ta.taskRole, ta.date, ta.startTime, ta.endTime, ta.taskProcess, ta.isApprove, ta.deleteAt) FROM " +
            "TaskAssigned ta LEFT JOIN ta.task tl LEFT JOIN ta.assignee e LEFT JOIN ta.taskRole tr LEFT JOIN " +
            "ta.taskProcess tp WHERE ta.deleteAt is null AND ta.isApprove = :approve AND (tl.taskName " +
            "LIKE %:keyword% OR e.name LIKE %:keyword% OR tr.taskRole LIKE %:keyword% OR tp.process " +
            "LIKE %:keyword%)")
    Page<TaskAssigned> taskAssignedSearch(@Param("approve")String approve, @Param("keyword") String keyword,
                                          Pageable pageable);

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.TaskAssigned (ta.id, ta.task, ta.assignee, " +
            "ta.taskRole, ta.date, ta.startTime, ta.endTime, ta.taskProcess, ta.isApprove, ta.deleteAt) FROM " +
            "TaskAssigned ta LEFT JOIN ta.task tl LEFT JOIN ta.assignee e LEFT JOIN ta.taskRole tr LEFT JOIN " +
            "ta.taskProcess tp WHERE ta.deleteAt is null AND ta.isApprove = 'Pending' AND ta.date >= curdate() " +
            "AND (tl.taskName LIKE %:keyword% OR e.name LIKE %:keyword% OR tr.taskRole LIKE %:keyword% OR " +
            "tp.process LIKE %:keyword%)")
    Page<TaskAssigned> taskAssignApprovalSearch(@Param("keyword") String keyword, Pageable pageable);

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.TaskAssigned (ta.id, ta.task, ta.assignee, " +
            "ta.taskRole, ta.date, ta.startTime, ta.endTime, ta.taskProcess, ta.isApprove, ta.deleteAt) FROM " +
            "TaskAssigned ta LEFT JOIN ta.task tl LEFT JOIN ta.assignee e LEFT JOIN ta.taskRole tr LEFT JOIN " +
            "ta.taskProcess tp WHERE ta.deleteAt is null AND tp.process != 'Complete' AND e.id = :userId " +
            "AND (tl.taskName LIKE %:keyword% OR e.name LIKE %:keyword% OR tr.taskRole LIKE %:keyword% OR " +
            "tp.process LIKE %:keyword%)")
    Page<TaskAssigned> getTaskRequestByUserSearch(@Param("keyword") String keyword,
                                                  @Param("userId") int userId, Pageable pageable);

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.TaskAssigned (ta.id, ta.task, ta.assignee, " +
            "ta.taskRole, ta.date, ta.startTime, ta.endTime, ta.taskProcess, ta.isApprove, ta.deleteAt) FROM " +
            "TaskAssigned ta LEFT JOIN ta.task tl LEFT JOIN ta.assignee e LEFT JOIN ta.taskRole tr LEFT JOIN " +
            "ta.taskProcess tp WHERE ta.deleteAt is null AND tp.process = 'Complete' AND e.id = :userId " +
            "AND (tl.taskName LIKE %:keyword% OR e.name LIKE %:keyword% OR tr.taskRole LIKE %:keyword% OR " +
            "tp.process LIKE %:keyword%)")
    Page<TaskAssigned> getTaskHistoryByUserSearch(@Param("keyword") String keyword,
                                                  @Param("userId") int userId, Pageable pageable);

    @Query(value = "SELECT EXISTS(SELECT ta.id from task_assigned ta WHERE ta.delete_at is null AND " +
            "ta.date = :dateTask AND ta.id_employee = :idEmployee AND (ta.start_time < :endTime AND " +
            "ta.end_time > :startTime))", nativeQuery = true)
    int checkScheduleConflict(@Param("idEmployee") int idEmployee, @Param("dateTask") LocalDate dateTask,
                              @Param("startTime")LocalTime startTime, @Param("endTime") LocalTime endTime);

    @Query(value = "SELECT EXISTS(SELECT ta.id from task_assigned ta WHERE ta.delete_at is null AND " +
            "ta.date = :dateTask AND ta.id_employee = :idEmployee AND ta.id != :idTaskAssignee AND " +
            "(ta.start_time < :endTime AND ta.end_time > :startTime))", nativeQuery = true)
    int checkScheduleConflictUpdate(@Param("idTaskAssignee") int idTaskAssignee,
                                    @Param("idEmployee") int idEmployee, @Param("dateTask") LocalDate dateTask,
                                    @Param("startTime")LocalTime startTime, @Param("endTime") LocalTime endTime);

    @Query(value = "SELECT EXISTS(SELECT ta.id from task_assigned ta WHERE ta.delete_at is null AND " +
            "ta.id_task = :idTask)", nativeQuery = true)
    int checkByTaskExists(@Param("idTask") int idTask);
}
