package id.co.nexsoft.Assignment.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(name = "employee",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
@NoArgsConstructor
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Size(max = 75)
    private String name;

    private String avatar;

    @NotNull
    @Size(max = 75)
    @Email
    private String email;

    @NotNull
    @Size(max = 30)
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String position;

    @NotNull
    private String role;

    @NotNull
    private String isApprove;

    private LocalDate createAt;
    private LocalDate EditAt;
    private LocalDate deleteAt;

    public Employee(String name, String email,  String username, String password,
                    String position, String role) {
        this.name = name;
        this.email = email;
        this.username = username;
        this.password = password;
        this.position = position;
        this.role = role;
    }
}
