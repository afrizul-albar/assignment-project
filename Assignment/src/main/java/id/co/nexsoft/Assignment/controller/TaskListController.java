package id.co.nexsoft.Assignment.controller;

import id.co.nexsoft.Assignment.dto.request.TaskListDTO;
import id.co.nexsoft.Assignment.services.TaskListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/taskList")
public class TaskListController {

    @Autowired
    private TaskListService service;

    @GetMapping("/findAll")
    public ResponseEntity<?> getAllTaskList() {
        return service.getAllTaskList();
    }

    @GetMapping("/findAllNoCondition")
    public ResponseEntity<?> fetAllTaskListNoCondition() {
        return service.getAllTaskListNoCondition();
    }

    @GetMapping("/findAllForForm")
    public ResponseEntity<?> getTaskListForForm() {
        return service.getTaskListForForm();
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<?> getTaskById(@PathVariable int id) {
        return service.getTaskListById(id);
    }

    @GetMapping("/findAllPagination")
    public ResponseEntity<?> getAllTaskPagination(@RequestParam(defaultValue = "0") int pageNo,
                                                  @RequestParam(defaultValue = "5") int pageSize,
                                                  @RequestParam(defaultValue = "") String keyword) {
        return service.getTaskListPagination(pageNo, pageSize, keyword);
    }

    @PostMapping("/createTaskList")
    public ResponseEntity<?> createTaskList(@Valid @RequestBody TaskListDTO taskListDTO) {
        return service.createTaskList(taskListDTO);
    }

    @PutMapping("/updateTaskList/{id}")
    public ResponseEntity<?> updateTaskList(@PathVariable int id,
                                            @Valid @RequestBody TaskListDTO taskListDTO) {
        return service.updateTaskList(id, taskListDTO);
    }

    @PutMapping("/deleteTaskList/{id}")
    public ResponseEntity<?> deleteTaskList(@PathVariable int id) {
        return service.deleteTaskList(id);
    }
}
