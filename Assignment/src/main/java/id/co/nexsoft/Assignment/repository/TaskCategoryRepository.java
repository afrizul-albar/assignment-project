package id.co.nexsoft.Assignment.repository;

import id.co.nexsoft.Assignment.entities.TaskCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskCategoryRepository extends CrudRepository<TaskCategory, Integer> {

    @Query(value = "SELECT c.id, c.task_category, c.delete_at from task_category c ", nativeQuery = true)
    List<TaskCategory> findAllNoCondition();

    @Query(value = "SELECT c.id, c.task_category, c.delete_at from task_category c " +
            "WHERE c.delete_at is null AND c.id = :id" , nativeQuery = true)
    TaskCategory findById(@Param("id") int id);

    @Query(value = "SELECT c.id, c.task_category, c.delete_at from task_category c " +
            "WHERE c.delete_at is null" , nativeQuery = true)
    List<TaskCategory> findAll();

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.TaskCategory (tc.id, tc.taskCategory, " +
            "tc.deleteAt) from TaskCategory tc where tc.deleteAt is null AND tc.taskCategory LIKE %:keyword%")
    Page<TaskCategory> categorySearch(@Param("keyword") String keyword, Pageable pageable);
}
