package id.co.nexsoft.Assignment.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TaskAssigned {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_task")
    private TaskList task;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employee assignee;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_taskRole")
    private TaskRole taskRole;

    @NotNull
    private LocalDate date;

    @NotNull
    private LocalTime startTime;

    @NotNull
    private LocalTime endTime;

    @ManyToOne
    @JoinColumn(name = "id_process")
    private TaskProcess taskProcess;

    @Column(columnDefinition = "varchar(20) default 'Pending'")
    private String isApprove;

    private LocalDate deleteAt;
}
