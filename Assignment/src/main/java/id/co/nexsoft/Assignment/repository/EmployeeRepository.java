package id.co.nexsoft.Assignment.repository;

import id.co.nexsoft.Assignment.dto.response.EmployeeDTO;
import id.co.nexsoft.Assignment.entities.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

    @Query(value = "SELECT e.id, e.name, e.avatar, e.email, e.username, e.password, e.position, e.role, " +
            "e.is_approve, e.create_at, e.edit_at, e.delete_at from employee e where e.delete_at is null AND " +
            "e.id = :id",
            nativeQuery = true)
    Employee findById(@Param("id") int id);

    @Query(value = "SELECT new id.co.nexsoft.Assignment.dto.response.EmployeeDTO (e.id, e.name, e.email, " +
            "e.username, e.position, e.avatar) from Employee e WHERE e.deleteAt is null AND e.id = :id")
    EmployeeDTO findByIdDTO(@Param("id") int id);

    @Query(value = "SELECT e.id, e.name, e.avatar, e.email, e.username, e.password, e.position, e.role, " +
            "e.is_approve, e.create_at, e.edit_at, e.delete_at from employee e where e.delete_at is null AND " +
            "e.username = :username",
            nativeQuery = true)
    Employee findByUsername(@Param("username") String username);

    @Query(value = "SELECT new id.co.nexsoft.Assignment.dto.response.EmployeeDTO (e.id, e.name, e.email, " +
            "e.username, e.position, e.avatar) from Employee e WHERE e.isApprove = 'Approved' AND " +
            "e.role = 'ROLE_USER' ORDER BY e.name ASC")
    List<EmployeeDTO> findAllNoCondition();

    @Query(value = "SELECT new id.co.nexsoft.Assignment.dto.response.EmployeeDTO (e.id, e.name, e.email, " +
            "e.username, e.position, e.avatar) from Employee e WHERE e.deleteAt is null AND e.isApprove = " +
            "'Approved' AND e.position = 'Leader' AND e.role = 'ROLE_USER' ORDER BY e.name ASC")
    List<EmployeeDTO> findEmployeeLeader();

    @Query(value = "SELECT new id.co.nexsoft.Assignment.dto.response.EmployeeDTO (e.id, e.name, e.email, " +
            "e.username, e.position, e.avatar) from Employee e WHERE e.deleteAt is null AND e.isApprove = " +
            "'Approved' AND e.role = 'ROLE_USER' ORDER BY e.name ASC")
    List<EmployeeDTO> findAllisApproved();

    @Query(value = "SELECT new id.co.nexsoft.Assignment.dto.response.EmployeeDTO (e.id, e.name, e.email, " +
            "e.username, e.position, e.avatar) from Employee e WHERE e.deleteAt is null AND e.role = 'ROLE_USER' " +
            "AND e.isApprove = :approval AND (e.name LIKE %:keyword% OR e.username LIKE %:keyword% OR e.email " +
            "LIKE %:keyword% OR e.position LIKE %:keyword%)")
    Page<EmployeeDTO> employeeIsApproveSearch(@Param("keyword") String keyword,
                                              @Param("approval") String approval, Pageable pageable);

    @Query(value = "SELECT EXISTS(SELECT e.id, e.name, e.avatar, e.email, e.username, e.password, e.position, " +
            "e.role, e.is_approve, e.create_at, e.edit_at, e.delete_at from employee e where e.id != :userId " +
            "AND e.username = :username)", nativeQuery = true)
    int checkExistsUsernameUpdate(@Param("userId") int userId, @Param("username") String username);

    @Query(value = "SELECT EXISTS(SELECT e.id, e.name, e.avatar, e.email, e.username, e.password, e.position, " +
            "e.role, e.is_approve, e.create_at, e.edit_at, e.delete_at from employee e where e.id != :userId " +
            "AND e.email = :email)", nativeQuery = true)
    int checkExistsEmailUpdate(@Param("userId") int userId, @Param("email") String email);

//    @Query(value = "SELECT EXISTS(SELECT e.id, e.name, e.avatar, e.email, e.username, e.password, e.position, " +
//            "e.role, e.isApprove, e.create_at, e.edit_at, e.delete_at from employee e where " +
//            "e.username = :username)")
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);

    @Query(value = "SELECT e.id, e.name, e.avatar, e.email, e.username, e.password, e.position, e.role, " +
            "e.is_approve, e.create_at, e.edit_at, e.delete_at from employee e where e.delete_at is null ",
            nativeQuery = true)
    List<Employee> findAll();
}
