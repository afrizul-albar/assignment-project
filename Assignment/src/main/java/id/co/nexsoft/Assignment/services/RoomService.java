package id.co.nexsoft.Assignment.services;

import id.co.nexsoft.Assignment.entities.Room;
import id.co.nexsoft.Assignment.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class RoomService {

    @Autowired
    private RoomRepository repo;

    public ResponseEntity<?> getAllRoomNoCondition() {
        return ResponseEntity.ok(repo.findAllNoCondition());
    }

    public ResponseEntity<?> getAllRoom() {
        return ResponseEntity.ok(repo.findAll());
    }

    public ResponseEntity<?> getRoomPagination(int pageNo, int pageSize, String keyword) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<Room> rooms = repo.roomSearch(keyword, pageable);
        return  ResponseEntity.ok(rooms);
    }

    public ResponseEntity<?> getRoomById(int id) {
        Room room = repo.findById(id);

        if (room == null) {
            return ResponseEntity.badRequest().body("Room not Found!");
        }

        return ResponseEntity.ok(room);
    }

    public ResponseEntity<?> createRoom(Room room) {
        Room data = new Room();
        data.setRoom(room.getRoom().trim());
        return ResponseEntity.ok(repo.save(data));
    }

    public ResponseEntity<?> updateRoom(int id, Room newData) {
        Room data = repo.findById(id);

        if (data == null) {
            return ResponseEntity.badRequest().body("Room Not Found!");
        }

        data.setRoom(newData.getRoom().trim());
        return ResponseEntity.ok(repo.save(data));
    }

    public ResponseEntity<?> deleteRoom(int id) {
        Room data = repo.findById(id);

        if (data == null) {
            return ResponseEntity.badRequest().body("Failed delete room!");
        }

        data.setDeleteAt(LocalDate.now());
        repo.save(data);
        return ResponseEntity.ok("Success delete Room with ID: " + id);
    }
}
