package id.co.nexsoft.Assignment.services;

import id.co.nexsoft.Assignment.entities.TaskProcess;
import id.co.nexsoft.Assignment.repository.TaskProcessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class TaskProcessService {

    @Autowired
    private TaskProcessRepository repo;

    public ResponseEntity<?> getALlTaskProcess() {
        return ResponseEntity.ok(repo.findAll());
    }

    public ResponseEntity<?> getProcessPagination(int pageNo, int pageSize, String keyword) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<TaskProcess> taskProcess = repo.processSearch(keyword, pageable);
        return ResponseEntity.ok(taskProcess);
    }

    public ResponseEntity<?> getTaskProcessById(int id) {
        TaskProcess taskProcess = repo.findById(id);

        if (taskProcess == null) {
            return ResponseEntity.badRequest().body("Task process not found!");
        }

        return ResponseEntity.ok(taskProcess);
    }

    public ResponseEntity<?> createTaskProcess(TaskProcess taskProcess) {
        return ResponseEntity.ok(repo.save(taskProcess));
    }

    public ResponseEntity<?> updateTaskProcess(int id, TaskProcess newData) {
        TaskProcess data = repo.findById(id);

        if (data == null || data.getDeleteAt() != null) {
            String message = data == null ? "Task process Not Found!" : "Task process already deleted";
            return ResponseEntity.badRequest().body(message);
        }

        data.setProcess(newData.getProcess());
        return ResponseEntity.ok(repo.save(data));
    }

    public ResponseEntity<?> deleteTaskProcess(int id) {
        TaskProcess data = repo.findById(id);

        if (data == null) {
            return ResponseEntity.badRequest().body("Failed delete task process!");
        }

        data.setDeleteAt(LocalDate.now());
        repo.save(data);
        return ResponseEntity.ok().body("Success delete Process with ID: " + id);
    }
}
