package id.co.nexsoft.Assignment.repository;

import id.co.nexsoft.Assignment.entities.TaskRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRoleRepository extends CrudRepository<TaskRole, Integer> {

    @Query(value = "SELECT r.id, r.task_role, r.delete_at from task_role r", nativeQuery = true)
    List<TaskRole> findAllNoCondition();

    @Query(value = "SELECT r.id, r.task_role, r.delete_at from " +
            "task_role r WHERE r.delete_at is null AND r.id = :id", nativeQuery = true)
    TaskRole findById(@Param("id") int id);

    @Query(value = "SELECT r.id, r.task_role, r.delete_at from " +
            "task_role r WHERE r.delete_at is null", nativeQuery = true)
    List<TaskRole> findAll();

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.TaskRole (tr.id, tr.taskRole, tr.deleteAt) " +
            "from TaskRole tr where tr.deleteAt is null AND tr.taskRole LIKE %:keyword%")
    Page<TaskRole> roleSearch(@Param("keyword") String keyword, Pageable pageable);
}
