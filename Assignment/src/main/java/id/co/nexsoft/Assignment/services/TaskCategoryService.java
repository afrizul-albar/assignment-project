package id.co.nexsoft.Assignment.services;

import id.co.nexsoft.Assignment.entities.TaskCategory;
import id.co.nexsoft.Assignment.repository.TaskCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class TaskCategoryService {

    @Autowired
    private TaskCategoryRepository repo;

    public ResponseEntity<?> getAllTaskCategoryNoCondition() {
        return ResponseEntity.ok(repo.findAllNoCondition());
    }

    public ResponseEntity<?> getAllTaskCategory() {
        return ResponseEntity.ok(repo.findAll());
    }

    public ResponseEntity<?> getTaskCategoryPagination(int pageNo, int pageSize, String keyword) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<TaskCategory> taskCategories = repo.categorySearch(keyword, pageable);
        return ResponseEntity.ok(taskCategories);
    }

    public ResponseEntity<?> getTaskCategoryById(int id) {
        TaskCategory taskCategory = repo.findById(id);

        if (taskCategory == null) {
            return ResponseEntity.badRequest().body("Task Category Not Found!");
        }

        return ResponseEntity.ok(taskCategory);
    }

    public ResponseEntity<?> createTaskCategory(TaskCategory taskCategory) {
        TaskCategory data = new TaskCategory();
        data.setTaskCategory(taskCategory.getTaskCategory().trim());
        return ResponseEntity.ok(repo.save(taskCategory));
    }

    public ResponseEntity<?> updateTaskCategory(int id, TaskCategory newData) {
        TaskCategory data = repo.findById(id);

        if (data == null) {
            return ResponseEntity.badRequest().body("Task Category Not Found!");
        }

        data.setTaskCategory(newData.getTaskCategory().trim());
        return ResponseEntity.ok(repo.save(data));
    }

    public ResponseEntity<?> deleteTaskCategory(int id) {
        TaskCategory data = repo.findById(id);

        if (data == null) {
            return ResponseEntity.badRequest().body("Failed delete task category!");
        }

        data.setDeleteAt(LocalDate.now());
        repo.save(data);
        return ResponseEntity.ok("Success delete Task Category with ID: " + id);
    }
}
