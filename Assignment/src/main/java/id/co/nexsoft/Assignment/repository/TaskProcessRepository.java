package id.co.nexsoft.Assignment.repository;

import id.co.nexsoft.Assignment.entities.TaskProcess;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskProcessRepository extends CrudRepository<TaskProcess, Integer> {

    @Query(value = "SELECT p.id, p.process, p.delete_at " +
            "from task_process p WHERE p.delete_at is null AND p.id = :id" , nativeQuery = true)
    TaskProcess findById(@Param("id") int id);

    @Query(value = "SELECT p.id, p.process, p.delete_at from task_process p WHERE " +
            "p.delete_at is null" , nativeQuery = true)
    List<TaskProcess> findAll();

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.TaskProcess (p.id, p.process, p.deleteAt) " +
            "from TaskProcess p where p.deleteAt is null AND p.process LIKE %:keyword%")
    Page<TaskProcess> processSearch(@Param("keyword") String keyword, Pageable pageable);
}
