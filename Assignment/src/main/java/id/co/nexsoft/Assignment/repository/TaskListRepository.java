package id.co.nexsoft.Assignment.repository;

import id.co.nexsoft.Assignment.entities.TaskList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TaskListRepository extends CrudRepository<TaskList, Integer> {
    @Query(value = "SELECT tl.id, tl.task_name, tl.start_date, tl.end_date, tl.id_room, tl.id_task_category, " +
            "tl.id_pic, tl.delete_at from task_list tl LEFT JOIN room r ON tl.id_room = r.id LEFT JOIN " +
            "task_category tc ON tl.id_task_category = tc.id LEFT JOIN employee e ON tl.id_pic = e.id " +
            "WHERE tl.delete_at is null AND tl.id = :id", nativeQuery = true)
    TaskList findById(@Param("id") int id);

    @Query(value = "SELECT tl.id, tl.task_name, tl.start_date, tl.end_date, tl.id_room, tl.id_task_category, " +
            "tl.id_pic, tl.delete_at from task_list tl LEFT JOIN room r ON tl.id_room = r.id LEFT JOIN " +
            "task_category tc ON tl.id_task_category = tc.id LEFT JOIN employee e ON tl.id_pic = e.id " +
            "WHERE tl.delete_at is null", nativeQuery = true)
    List<TaskList> findAll();

    @Query(value = "SELECT tl.id, tl.task_name, tl.start_date, tl.end_date, tl.id_room, tl.id_task_category, " +
            "tl.id_pic, tl.delete_at from task_list tl LEFT JOIN room r ON tl.id_room = r.id LEFT JOIN " +
            "task_category tc ON tl.id_task_category = tc.id LEFT JOIN employee e ON tl.id_pic = e.id ",
            nativeQuery = true)
    List<TaskList> findAllNoCondition();

    @Query(value = "SELECT tl.id, tl.task_name, tl.start_date, tl.end_date, tl.id_room, tl.id_task_category, " +
            "tl.id_pic, tl.delete_at from task_list tl LEFT JOIN room r ON tl.id_room = r.id LEFT JOIN " +
            "task_category tc ON tl.id_task_category = tc.id LEFT JOIN employee e ON tl.id_pic = e.id " +
            "WHERE tl.delete_at is null AND tl.end_date >= curdate()", nativeQuery = true)
    List<TaskList> findTaskListForm();

    @Query(value = "SELECT new id.co.nexsoft.Assignment.entities.TaskList (tl.id, tl.taskName, tl.startDate, " +
            "tl.endDate, tl.room, tl.taskCategory, tl.pic, tl.deleteAt) from TaskList tl LEFT JOIN tl.room r " +
            "LEFT JOIN tl.taskCategory tc LEFT JOIN tl.pic e WHERE tl.deleteAt is null AND (tl.taskName LIKE " +
            "%:keyword% OR r.room LIKE %:keyword% OR tc.taskCategory LIKE %:keyword% OR e.name LIKE %:keyword%)")
    Page<TaskList> taskListSearch(@Param("keyword") String keyword, Pageable pageable);

    @Query(value = "SELECT EXISTS(SELECT tl.id from task_list tl WHERE tl.delete_at is null AND " +
            "tl.id_room = :idRoom AND (tl.start_date < :endDate AND tl.end_date > :startDate))",
            nativeQuery = true)
    int checkRoomConflict(@Param("idRoom") int idRoom, @Param("startDate")LocalDate startDate,
                          @Param("endDate")LocalDate endDate);

    @Query(value = "SELECT EXISTS(SELECT tl.id from task_list tl WHERE tl.delete_at is null AND " +
            "tl.id_room = :idRoom AND tl.id != :idTask AND (tl.start_date < :endDate AND tl.end_date > " +
            ":startDate))", nativeQuery = true)
    int checkRoomConflictUpdate(@Param("idTask") int idTask, @Param("idRoom") int idRoom,
                                @Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate);
}
