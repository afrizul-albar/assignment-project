package id.co.nexsoft.Assignment.services;

import id.co.nexsoft.Assignment.dto.request.TaskAssigneeDTO;
import id.co.nexsoft.Assignment.dto.request.TaskAssigneeRequest;
import id.co.nexsoft.Assignment.entities.*;
import id.co.nexsoft.Assignment.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

@Service
public class TaskAssignedService {

    @Autowired
    private TaskAssignedRepository taskAssignedRepository;

    @Autowired
    private TaskListRepository taskListRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TaskRoleRepository taskRoleRepository;

    @Autowired
    private TaskProcessRepository taskProcessRepository;

    @Autowired
    EmailService emailService;

    public ResponseEntity<?> getAllTaskAssigneed() {
        return ResponseEntity.ok(taskAssignedRepository.findAll());
    }

    public ResponseEntity<?> getAllTaskAssigneedByApproval(String approval) {
        return ResponseEntity.ok(taskAssignedRepository.findAllByApproval(approval));
    }

    public ResponseEntity<?> getAllSchedule() {
        return ResponseEntity.ok(taskAssignedRepository.findAllSchedule());
    }

    public ResponseEntity<?> getScheduleByUser(int idUser) {
        return ResponseEntity.ok(taskAssignedRepository.findScheduleByUser(idUser));
    }

    public ResponseEntity<?> getTaskReport(int pageNo, int pageSize, String keywordTaskName, String keywordPIC,
                                           String keywordAssignee, String keywordRoom) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("date").descending());
        Page<TaskAssigned> taskAssigned = taskAssignedRepository.getTaskReport(keywordTaskName, keywordPIC,
                                                                    keywordAssignee, keywordRoom, pageable);
        return ResponseEntity.ok(taskAssigned);
    }

    public ResponseEntity<?> getTaskReportToCSV(String keywordTaskName, String keywordPIC,
                                                String keywordAssignee, String keywordRoom) {
        return ResponseEntity.ok(taskAssignedRepository.getTaskReportToCSV(keywordTaskName, keywordPIC,
                                                                            keywordAssignee, keywordRoom));
    }

    public ResponseEntity<?> getTaskAssignedPagination(int pageNo, int pageSize, String keyword,
                                                       String approve) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("date").descending());
        Page<TaskAssigned> taskAssigned = taskAssignedRepository.taskAssignedSearch(approve, keyword, pageable);
        return ResponseEntity.ok(taskAssigned);
    }

    public ResponseEntity<?> getTaskApprovalPagination(int pageNo, int pageSize, String keyword) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("date").ascending());
        Page<TaskAssigned> taskAssigned = taskAssignedRepository.taskAssignApprovalSearch(keyword, pageable);
        return ResponseEntity.ok(taskAssigned);
    }

    public ResponseEntity<?> getAllTaskAssigneedByToday(int pageNo,int pageSize, String keyword) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("date").ascending());
        Page<TaskAssigned> taskAssigned = taskAssignedRepository.findTaskToAssignSearch(keyword, pageable);
        return ResponseEntity.ok(taskAssigned);
    }

    public ResponseEntity<?> getAllTaskRequestByUser(int pageNo, int pageSize, int userId, String keyword) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<TaskAssigned> taskAssigned = taskAssignedRepository
                                           .getTaskRequestByUserSearch(keyword, userId, pageable);
        return ResponseEntity.ok(taskAssigned);
    }

    public ResponseEntity<?> getAllTaskHistoryByUser(int pageNo, int pageSize, int userId, String keyword) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<TaskAssigned> taskAssigned = taskAssignedRepository
                                            .getTaskHistoryByUserSearch(keyword, userId, pageable);
        return ResponseEntity.ok(taskAssigned);
    }

    public ResponseEntity<?> getTaskAssigneedById(int id) {
        TaskAssigned taskAssigned = taskAssignedRepository.findById(id);

        if (taskAssigned == null) {
            return ResponseEntity.badRequest().body("Task assigneed not found!");
        }
        return ResponseEntity.ok(taskAssigned);
    }

    public ResponseEntity<?> createTaskAssigneed(TaskAssigneeDTO taskAssigneeDTO) {
        TaskList dataTask = taskListRepository.findById(taskAssigneeDTO.getTaskId());
        Employee dataAssignee = employeeRepository.findById(taskAssigneeDTO.getAssigneeId());
        TaskRole dataRole = taskRoleRepository.findById(taskAssigneeDTO.getRoleId());
        TaskProcess dataProcess = taskProcessRepository.findById(1);

        if (dataTask == null ||  dataRole == null || dataProcess == null) {
            return ResponseEntity.badRequest().body("Bad Request!");
        }

        if (taskAssigneeDTO.getEndTime().isBefore(taskAssigneeDTO.getStartTime()) ||
            taskAssigneeDTO.getStartTime().isAfter(taskAssigneeDTO.getEndTime())) {
            String message = taskAssigneeDTO.getEndTime().isBefore(taskAssigneeDTO.getStartTime())
                    ? "End time can't be less than start time!"
                    : "Start time can't be more than end time!";
            return ResponseEntity.badRequest().body(message);
        }

        if (taskAssigneeDTO.getDate().isBefore(LocalDate.now())
                || taskAssigneeDTO.getDate().isAfter(dataTask.getEndDate())) {
            return ResponseEntity.badRequest().body("Date can't be less than today and more " +
                    "than end date task!");
        }

        if (dataTask.getEndDate().isBefore(LocalDate.now())) {
            return ResponseEntity.badRequest().body("Task is expired! please choose another task!");
        }

        TaskAssigned dataTaskAssignee = new TaskAssigned();
        dataTaskAssignee.setTask(dataTask);
        dataTaskAssignee.setTaskRole(dataRole);
        dataTaskAssignee.setTaskProcess(dataProcess);
        dataTaskAssignee.setDate(taskAssigneeDTO.getDate());
        dataTaskAssignee.setStartTime(taskAssigneeDTO.getStartTime());
        dataTaskAssignee.setEndTime(taskAssigneeDTO.getEndTime());
        if (dataAssignee != null) {
            if (dataAssignee.getIsApprove().equalsIgnoreCase("Pending") ||
                dataAssignee.getRole().equalsIgnoreCase("ROLE_ADMIN")) {
                String message = dataAssignee.getIsApprove().equalsIgnoreCase("Pending")
                        ? "User has not been approved! Choose another user!"
                        : "Admin can't assign task!";
                return ResponseEntity.badRequest().body(message);
            }

            if (taskAssignedRepository.checkScheduleConflict(dataAssignee.getId(), taskAssigneeDTO.getDate(),
                    taskAssigneeDTO.getStartTime(), taskAssigneeDTO.getEndTime()) == 1) {
                return ResponseEntity.badRequest().body("Asignee already have a schedule at this time. " +
                        "Please choose another Assignee.");
            }

            dataTaskAssignee.setAssignee(dataAssignee);
            dataTaskAssignee.setIsApprove("Approved");
        } else {
            dataTaskAssignee.setIsApprove("Pending");
        }

        taskAssignedRepository.save(dataTaskAssignee);
        return ResponseEntity.ok("Success create task Assignee with task : " + dataTask.getTaskName());
    }

    public ResponseEntity<?> updateTaskAssignee(int id, TaskAssigneeDTO taskAssigneeDTO) {
        TaskAssigned dataTaskAssigned = taskAssignedRepository.findById(id);
        TaskList dataTask = taskListRepository.findById(taskAssigneeDTO.getTaskId());
        Employee dataAssignee = employeeRepository.findById(taskAssigneeDTO.getAssigneeId());
        TaskRole dataRole = taskRoleRepository.findById(taskAssigneeDTO.getRoleId());
        TaskProcess dataProcess = taskProcessRepository.findById(taskAssigneeDTO.getProcessId());

        if (dataTaskAssigned== null || dataTask == null ||  dataRole == null || dataProcess == null) {
            return ResponseEntity.badRequest().body("Bad Request!");
        }

        if (dataTaskAssigned.getTaskProcess().getProcess().equalsIgnoreCase("Complete")) {
            return ResponseEntity.badRequest().body("Task is already complete!");
        }

        if (taskAssigneeDTO.getEndTime().isBefore(taskAssigneeDTO.getStartTime()) ||
                taskAssigneeDTO.getStartTime().isAfter(taskAssigneeDTO.getEndTime())) {
            String message = taskAssigneeDTO.getEndTime().isBefore(taskAssigneeDTO.getStartTime())
                    ? "End time can't be less than start time!"
                    : "Start time can't be more than end time!";
            return ResponseEntity.badRequest().body(message);
        }

        if (taskAssigneeDTO.getDate().isBefore(LocalDate.now())
                || taskAssigneeDTO.getDate().isAfter(dataTask.getEndDate())) {
            return ResponseEntity.badRequest().body("Date can't be less than today and more " +
                    "than end date task!");
        }

        if (dataTask.getEndDate().isBefore(LocalDate.now())) {
            return ResponseEntity.badRequest().body("Task is expired! please choose another task!");
        }

        if (dataTaskAssigned.getDate().isBefore(LocalDate.now())) {
            return ResponseEntity.badRequest().body("Task Assignee is expired!");
        }

        dataTaskAssigned.setTask(dataTask);
        dataTaskAssigned.setTaskRole(dataRole);
        dataTaskAssigned.setDate(taskAssigneeDTO.getDate());
        dataTaskAssigned.setStartTime(taskAssigneeDTO.getStartTime());
        dataTaskAssigned.setEndTime(taskAssigneeDTO.getEndTime());
        dataTaskAssigned.setTaskProcess(dataProcess);
        if (dataAssignee != null) {

            if (dataAssignee.getIsApprove().equalsIgnoreCase("Pending") ||
                    dataAssignee.getRole().equalsIgnoreCase("ROLE_ADMIN")) {
                String message = dataAssignee.getIsApprove().equalsIgnoreCase("Pending")
                        ? "User has not been approved! Choose another user!"
                        : "Admin can't assign task!";
                return ResponseEntity.badRequest().body(message);
            }

            if (taskAssignedRepository.checkScheduleConflictUpdate(id, dataAssignee.getId(),
                taskAssigneeDTO.getDate(), taskAssigneeDTO.getStartTime(), taskAssigneeDTO.getEndTime()) == 1)
            {
                return ResponseEntity.badRequest().body("Asignee already have a schedule at this time. " +
                        "Please choose another Assignee.");
            }
            dataTaskAssigned.setAssignee(dataAssignee);
            dataTaskAssigned.setIsApprove("Approved");
        } else {
            dataTaskAssigned.setIsApprove("Pending");
        }

        taskAssignedRepository.save(dataTaskAssigned);
        return ResponseEntity.ok("Success update task Assignee with task : " + dataTask.getTaskName());
    }

    public ResponseEntity<?> updateTaskAssignedByUser(int id, TaskAssigneeRequest request) {
        TaskAssigned dataTaskAssigned = taskAssignedRepository.findById(id);
        TaskProcess dataProcess = taskProcessRepository.findById(request.getProcessId());

        if (dataTaskAssigned == null) {
            return ResponseEntity.badRequest().body("Task assignee not found!");
        }

        if (dataTaskAssigned.getDate().isBefore(LocalDate.now())) {
            return ResponseEntity.badRequest().body("Task Assignee is expired!");
        }

        if (dataTaskAssigned.getAssignee() == null || dataTaskAssigned.getIsApprove()
                .equalsIgnoreCase("Pending")) {

            if (request.getDate() == null || request.getStartTime() == null || request.getEndTime() == null) {
                return ResponseEntity.badRequest().body("Bad Request!");
            }

            if (request.getEndTime().isBefore(request.getStartTime()) ||
                request.getStartTime().isAfter(request.getEndTime())) {
                String message = request.getEndTime().isBefore(request.getStartTime())
                        ? "End time can't be less than start time!"
                        : "Start time can't be more than end time!";
                return ResponseEntity.badRequest().body(message);
            }

            if (request.getDate().isBefore(LocalDate.now())
                    || request.getDate().isAfter(dataTaskAssigned.getTask().getEndDate())) {
                return ResponseEntity.badRequest().body("Date can't be less than today and more " +
                        "than end date task!");
            }

            if (dataTaskAssigned.getAssignee() != null) {
                if (taskAssignedRepository.checkScheduleConflictUpdate(id, dataTaskAssigned.getAssignee().getId(),
                        request.getDate(), request.getStartTime(), request.getEndTime()) == 1) {
                    return ResponseEntity.badRequest().body("Asignee already have a schedule at this time. " +
                            "Please choose another date or time!.");
                }
            }

            dataTaskAssigned.setDate(request.getDate());
            dataTaskAssigned.setStartTime(request.getStartTime());
            dataTaskAssigned.setEndTime(request.getEndTime());
            taskAssignedRepository.save(dataTaskAssigned);
            return ResponseEntity.ok("Success update task assigned with Task : " + dataTaskAssigned.getTask()
                                    .getTaskName());
        } else if (dataTaskAssigned.getAssignee() != null && dataTaskAssigned.getTaskProcess().
                getProcess().equalsIgnoreCase("Pre-Process")) {

            if (request.getDate() == null || request.getStartTime() == null ||
                request.getEndTime() == null || dataProcess == null) {
                return ResponseEntity.badRequest().body("Bad Request!");
            }

            if (request.getDate().isBefore(LocalDate.now())) {
                return ResponseEntity.badRequest().body("Date can't less than today!");
            }

            if (request.getEndTime().isBefore(request.getStartTime()) ||
                    request.getStartTime().isAfter(request.getEndTime())) {
                String message = request.getEndTime().isBefore(request.getStartTime())
                        ? "End time can't be less than start time!"
                        : "Start time can't be more than end time!";
                return ResponseEntity.badRequest().body(message);
            }

            if (request.getDate().isBefore(LocalDate.now())
                    || request.getDate().isAfter(dataTaskAssigned.getTask().getEndDate())) {
                return ResponseEntity.badRequest().body("Date can't be less than today and more " +
                        "than end date task!");
            }

            if (taskAssignedRepository.checkScheduleConflictUpdate(id, dataTaskAssigned.getAssignee().getId(),
                    request.getDate(), request.getStartTime(), request.getEndTime()) == 1) {
                return ResponseEntity.badRequest().body("Asignee already have a schedule at this time. " +
                        "Please choose another date or time!.");
            }

            dataTaskAssigned.setDate(request.getDate());
            dataTaskAssigned.setStartTime(request.getStartTime());
            dataTaskAssigned.setEndTime(request.getEndTime());
            dataTaskAssigned.setTaskProcess(dataProcess);
            taskAssignedRepository.save(dataTaskAssigned);
            return ResponseEntity.ok("Success update task assigned with Task : " + dataTaskAssigned.getTask()
                    .getTaskName());
        } else if (dataTaskAssigned.getAssignee() != null && dataTaskAssigned.getTaskProcess()
                .getProcess().equalsIgnoreCase("Process")) {

            if (dataProcess == null) {
                return ResponseEntity.badRequest().body("Process cannot be empty!");
            }

            dataTaskAssigned.setTaskProcess(dataProcess);
            taskAssignedRepository.save(dataTaskAssigned);
            return ResponseEntity.ok("Success update task assigned with Task : " + dataTaskAssigned.getTask()
                    .getTaskName());
        } else {
            return ResponseEntity.badRequest().body("Task Assigned already completed!");
        }
    }

    public ResponseEntity<?> approvalTaskAssigned(int idTask, String approval) {
        TaskAssigned dataTaskAssigned = taskAssignedRepository.findById(idTask);
        String to = "";
        String topic = "Approval Task Assignee";
        String body = "";

        if (dataTaskAssigned == null) {
            return ResponseEntity.badRequest().body("Task Assignee not found!");
        }

        if (dataTaskAssigned.getAssignee() == null) {
            return ResponseEntity.badRequest().body("Assignee is still empty!");
        }

        if (dataTaskAssigned.getDate().isBefore(LocalDate.now())) {
            return ResponseEntity.badRequest().body("Task is expired!");
        }

        if (dataTaskAssigned.getIsApprove().equalsIgnoreCase("Approved")) {
            return ResponseEntity.badRequest().body("Task already approved!");
        }

        to = dataTaskAssigned.getAssignee().getEmail();
        if ("Approved".equalsIgnoreCase(approval)){
            body = "== APPROVAL TASK ASSIGNEE == \n" +
                    "Your request for an assignment by \n" +
                    "ID : " + dataTaskAssigned.getId() + "\n" +
                    "Task Name : " + dataTaskAssigned.getTask().getTaskName() + "\n" +
                    "Start Date : " + dataTaskAssigned.getDate() + "\n" +
                    "Start Time : " + dataTaskAssigned.getStartTime() + "\n" +
                    "End Time : " + dataTaskAssigned.getEndTime() + "\n" +
                    "is Approved. Please prepare yourself. Thank you.";
            dataTaskAssigned.setIsApprove("Approved");
            taskAssignedRepository.save(dataTaskAssigned);
            emailService.sendEmail(to, body, topic);
            return ResponseEntity.ok("Success Approve Task with ID : " + idTask);
        } else if ("Rejected".equalsIgnoreCase(approval)){
            body = "== APPROVAL TASK ASSIGNEE == \n" +
                    "Your request for an assignment by \n" +
                    "ID : " + dataTaskAssigned.getId() + "\n" +
                    "Task Name : " + dataTaskAssigned.getTask().getTaskName() + "\n" +
                    "is Rejected. Maybe you can take another assignment. Forgive us and thank you..";
            taskAssignedRepository.delete(dataTaskAssigned);
            emailService.sendEmail(to, body, topic);
            return ResponseEntity.ok("Success Reject & Delete Task Assignee with ID : " + idTask);
        } else {
            return ResponseEntity.badRequest().body("Bad Request! Option not Found!");
        }
    }

    public ResponseEntity<?> assignEmployee(int idTask, int idEmployee) {
        TaskAssigned dataTaskAssignee = taskAssignedRepository.findById(idTask);
        Employee dataAssignee = employeeRepository.findById(idEmployee);

        if (dataTaskAssignee == null || dataAssignee == null) {
            return ResponseEntity.badRequest().body("Bad Request!");
        }

        if (dataTaskAssignee.getDate().isBefore(LocalDate.now())) {
            return ResponseEntity.badRequest().body("Task is expired! Please choose another task!");
        }

        if (dataTaskAssignee.getAssignee() != null && dataTaskAssignee.getIsApprove() != "Pending") {
            return ResponseEntity.badRequest().body("Someone already applied this task! Please choose" +
                    " Another Task!");
        }

        if (taskAssignedRepository.checkScheduleConflict(idEmployee, dataTaskAssignee.getDate(),
                dataTaskAssignee.getStartTime(), dataTaskAssignee.getEndTime()) == 1) {
            return ResponseEntity.badRequest().body("Asignee already have a schedule at that time. " +
                    "Please choose another Task.");
        }

        dataTaskAssignee.setAssignee(dataAssignee);
        taskAssignedRepository.save(dataTaskAssignee);
        return ResponseEntity.ok("Success assign task with ID : " + idTask);
    }

    public ResponseEntity<?> deleteTaskAssigned(int id) {
        TaskAssigned taskAssigned = taskAssignedRepository.findById(id);

        if (taskAssigned == null) {
            return ResponseEntity.badRequest().body("Failed delete task assignee!");
        }

        if ("Complete".equalsIgnoreCase(taskAssigned.getTaskProcess().getProcess())) {
            return ResponseEntity.badRequest().body("You can't delete a completed task!");
        }

        if (taskAssigned.getAssignee() != null) {
            return ResponseEntity.badRequest().body("Can't delete task!");
        }

        taskAssigned.setDeleteAt(LocalDate.now());
        taskAssignedRepository.save(taskAssigned);
        return ResponseEntity.ok("Success delete task assignee with id " + id);
    }

}
