package id.co.nexsoft.Assignment.controller;

import id.co.nexsoft.Assignment.entities.Room;
import id.co.nexsoft.Assignment.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/room")
public class RoomController {

    @Autowired
    private RoomService service;

    @GetMapping("/findAllNoCondition")
    public ResponseEntity<?> getAllRoomNoCOndition() {
        return service.getAllRoomNoCondition();
    }

    @GetMapping("/findAll")
    public ResponseEntity<?> getAllRoom() {
        return service.getAllRoom();
    }

    @GetMapping("/findAllPagination")
    public ResponseEntity<?> getAllRoomPagination(@RequestParam(defaultValue = "0") int pageNo,
                                                  @RequestParam(defaultValue = "5") int pageSize,
                                                  @RequestParam(defaultValue = "") String keyword) {
        return service.getRoomPagination(pageNo, pageSize, keyword);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<?> getRoomById(@PathVariable int id) {
        return service.getRoomById(id);
    }

    @PostMapping("/createRoom")
    public ResponseEntity<?> createRoom(@Valid @RequestBody Room room) {
        return service.createRoom(room);
    }

    @PutMapping("/updateRoom/{id}")
    public ResponseEntity<?> updateRoom(@PathVariable int id, @Valid @RequestBody Room room) {
        return service.updateRoom(id, room);
    }

    @PutMapping("/deleteRoom/{id}")
    public ResponseEntity<?> deleteRoom(@PathVariable int id) {
        return service.deleteRoom(id);
    }
}
