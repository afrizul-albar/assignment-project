import Main  from'../components/Dashboard/Admin/Main/Main';
import DataEmployee  from'../components/Dashboard/Admin/Data/Employee';
import DataCategory  from'../components/Dashboard/Admin/Data/Category';
import DataProcess  from'../components/Dashboard/Admin/Data/Process';
import DataRole  from'../components/Dashboard/Admin/Data/Role';
import DataRoom  from'../components/Dashboard/Admin/Data/Room';
import DataTask  from'../components/Dashboard/Admin/Data/TaskList';
import DataAssigneed  from'../components/Dashboard/Admin/Data/TaskAssignee';
import DetailTask from '../components/Dashboard/Admin/Details/DetailTask';
import DetailAssignee from '../components/Dashboard/Admin/Details/DetailAssignee';
import DetailEmployee from '../components/Dashboard/Admin/Details/DetailEmployee';
import AppEmployee  from'../components/Dashboard/Admin/Approval/Employee';
import AppAssignee  from'../components/Dashboard/Admin/Approval/TaskAssignee';
import Report  from'../components/Dashboard/Admin/Report/Report';
import Profile from '../components/Dashboard/Profile/Profile';

const AdminRoutes = [
    {path: '/', exact: true, name: 'Default'},
    {path: '/home',  name: 'Main Page', component: Main},
    {path: '/data/employee', name: 'Data Employee Page', component: DataEmployee},
    {path: '/data/process', name: 'Data Process Page', component: DataProcess},
    {path: '/data/room', name: 'Data Room Page', component: DataRoom},
    {path: '/data/role', name: 'Data Role Page', component: DataRole},
    {path: '/data/category', name: 'Data Category Page', component: DataCategory},
    {path: '/data/taskList', name: 'Data Task Page', component: DataTask},
    {path: '/data/taskAssigned', name: 'Data Task Assigneed Page', component: DataAssigneed},
    {path: '/detail/task', name: 'Detail Task Page', component: DetailTask},
    {path: '/detail/assignee', name: 'Detail Task Assignee', component: DetailAssignee},
    {path: '/detail/employee', name: 'Detail Employee Page', component: DetailEmployee},
    {path: '/approval/employee', name: 'Approval Employee Page', component: AppEmployee},
    {path: '/approval/taskAssignee', name: 'Approval Task Assigneed Page', component: AppAssignee},
    {path: '/report', name: 'Report Page', component: Report},
    {path: '/profile', name: 'Profile Page', component: Profile} 
]

export default AdminRoutes;