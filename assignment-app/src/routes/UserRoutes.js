import Main from "../components/Dashboard/User/Main/Main";
import Assign from "../components/Dashboard/User/Assign/Assign";
import History from "../components/Dashboard/User/History/History";
import DetailTask from "../components/Dashboard/User/Details/DetailTask";
import Profile from "../components/Dashboard/Profile/Profile";
import Request from "../components/Dashboard/User/Request/Request";

const UserRoutes = [
    {path: '/', exact: true, name: 'Default'},
    {path: '/home',  name: 'Main Page', component: Main},
    {path: '/assign',  name: 'Assign Page', component: Assign},
    {path: '/history',  name: 'History Page', component: History},
    {path: '/request',  name: 'Request Page', component: Request},
    {path: '/detail', name: "Detail Page", component: DetailTask},
    {path: '/profile', name: "Profile Page", component: Profile}
]

export default UserRoutes;