import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, Route, Switch } from "react-router";
import AdminRoutes from "./AdminRoutes";
import UserRoutes from "./UserRoutes";
import jwtDecode from "jwt-decode";
import { authAction } from "../redux/reducer/AuthReducer";
import swal from "sweetalert";

const Content = () => {
  const token = useSelector((state) => state.token);
  const role = useSelector((state) => state.role);
  const dispatch = useDispatch();

  if (token) {
    const decoded = jwtDecode(token);
    if (decoded.exp < Date.now() / 1000) {
      swal({ icon: "info", title: "Info!", text: "Token expired! Please login again!" });
      dispatch(authAction.logout());
    }
  }

  const AdminRoute = ({ ...props }) => {
    return token && role === "ROLE_ADMIN" ? (
      <Route {...props} />
    ) : (
      <Redirect to="/login" />
    );
  };

  const UserRoute = ({ ...props }) => {
    return token && role === "ROLE_USER" ? (
      <Route {...props} />
    ) : (
      <Redirect to="/login" />
    );
  };

  return (
    <Switch>
      {role === "ROLE_ADMIN"
        ? AdminRoutes.map((route, idx) => {
            return (
              route.component && (
                <AdminRoute
                  key={idx}
                  path={route.path}
                  exact={route.exact}
                  name={route.name}
                  render={(props) => <route.component {...props} />}
                />
              )
            );
          })
        : role === "ROLE_USER" 
        ? UserRoutes.map((route, idx) => {
            return (
              route.component && (
                <UserRoute
                  key={idx}
                  path={route.path}
                  exact={route.exact}
                  name={route.name}
                  render={(props) => <route.component {...props} />}
                />
              )
            );
          })
        : <Redirect to="/login" />
        }
      <Redirect exact from="/" to="/home" />
      <Redirect from="*" to="/404" />
    </Switch>
  );
};

export default Content;
