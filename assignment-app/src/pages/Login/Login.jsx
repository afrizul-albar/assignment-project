import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import swal from "sweetalert";
import InputText from "../../components/Input/InputText";
import { authAction } from "../../redux/reducer/AuthReducer";
import { postNoRestrict } from "../../service/Fetch";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();
  let history = useHistory();

  const loginHandler = (e) => {
      e.preventDefault();

      let data = {
          username : username,
          password : password
      }

      postNoRestrict("employee/login", data)
      .then(response => {
          dispatch(authAction.login(response.data))
          swal({ icon: "success", title: "Success!", text: "Login Success!" });
          history.push("/")
      })
      .catch(err => {
        swal({ icon: "error", title: "Error!", text: err.response.data });
      })
  } 

  return (
    <div className="container">
      <div className="row m-3">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div className="card border-0 shadow rounded-3 my-5">
            <div className="card-body p-4 p-sm-5">
              <h5 className="card-title text-center mb-5 fw-light fs-5">Sign In</h5>
              <form onSubmit={loginHandler}>
                <div className="form-floating mb-3">
                  <input
                    type="text"
                    className="form-control"
                    id="floatingInput"
                    name="username"
                    placeHolder="Username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    required={true}
                  />
                  <label htmlFor="floatingInput">Username</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    type="password"
                    className="form-control"
                    id="floatingPassword"
                    name="password"
                    placeHolder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required={true}
                  />
                  <label htmlFor="floatingPassword">Password</label>
                </div>
                <div className="d-grid">
                  <button
                    className="btn btn-warning btn-login text-uppercase fw-bold text-white"
                    type="submit"
                  >
                    Sign in
                  </button>
                </div>
                <div class="form-group col-lg-12 mx-auto d-flex align-items-center my-4">
                  <div class="border-bottom w-100 ml-5"></div>
                  <span class="px-2 small text-muted font-weight-bold text-muted">
                    OR
                  </span>
                  <div class="border-bottom w-100 mr-5"></div>
                </div>
                <div class="text-center w-100">
                  <p class="text-muted font-weight-bold">
                    Don't have an account yet? 
                    <Link to="/register" className="text-primary ml-2">
                      Register
                    </Link>
                  </p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
