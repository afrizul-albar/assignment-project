import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Redirect, useHistory } from "react-router";
import Sidebar from "../../components/Sidebar/Sidebar";
import Content from "../../routes/Content";

export default function Home() {
  const token = useSelector((state) => state.token);
  const history = useHistory();

  return (
    <>
      {token ? (
        <div className="container-fluid">
          <div className="row flex-nowrap">
            <Sidebar />
            <div className="col py-3">
              <Content />
            </div>
          </div>
        </div>
      ) : (
        <Redirect to="/login" />
      )}
    </>
  );
}
