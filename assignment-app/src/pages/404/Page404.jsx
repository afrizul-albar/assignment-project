import React from "react";
import { Link } from "react-router-dom";

export default function Page404() {
  return (
    <div className="d-flex align-items-center">
      <div className="container my-5">
        <div className="row justify-content-center">
          <div className="col-md-6">
            <div className="clearfix">
              <h1 className="float-left display-3 mr-4">404</h1>
              <h4 className="pt-3">Opps! You're lost.</h4>
              <p className="text-muted float-left">
                The page you are looking for was not found.
              </p>
              <p><Link to="/">Back to Home Page</Link></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
