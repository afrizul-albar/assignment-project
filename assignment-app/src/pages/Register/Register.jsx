import React, { useState } from "react";
import { useHistory } from "react-router";
import { postNoRestrict } from "../../service/Fetch";
import swal from "sweetalert";
import { Link } from "react-router-dom";

export default function Register() {
  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [retypePassword, setRetypePassword] = useState();
  const [position, setPosition] = useState();

  const history = useHistory();

  const registerHandler = (e) => {
    e.preventDefault();

    const data = {
      name: name,
      email: email,
      username: username,
      password: password,
      position: position,
    };

    postNoRestrict(`employee/register`, data)
      .then((response) => {
        swal({
          title: "Success!",
          text: response.data,
          icon: "success",
        });
        history.push("/login");
      })
      .catch((err) => {
        swal({
          title: "Error!",
          text: err.response.data,
          icon: "error",
        });
      });
  };

  return (
    <div className="container">
      <div className="row m-3">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div className="card border-0 shadow rounded-3 my-5">
            <div className="card-body p-4 p-sm-5">
              <h5 className="card-title text-center mb-5 fw-light fs-5">
                Register
              </h5>
              <form onSubmit={registerHandler}>
                <div className="form-floating mb-3">
                  <input
                    type="text"
                    className="form-control"
                    id="floatingInput"
                    name="name"
                    placeHolder="Name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    required
                  />
                  <label htmlFor="floatingInput">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    type="email"
                    className="form-control"
                    id="floatingInput"
                    name="email"
                    placeHolder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                  <label htmlFor="floatingInput">Email</label>
                </div>
                <div className="form-floating mb-3">
                  <select
                    id="inputPosition"
                    className="form-select"
                    value={position}
                    onChange={(e) => setPosition(e.target.value)}
                    required
                  >
                    <option value={null} className="text-muted">
                      Choose Position...
                    </option>
                    <hr className="dropdown-divider" />
                    <option value="Leader">Leader</option>
                    <option value="Employee">Employee</option>
                  </select>
                  <label htmlFor="floatingInput">Position</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    type="text"
                    className="form-control"
                    id="floatingInput"
                    name="username"
                    placeHolder="Username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    required
                  />
                  <label htmlFor="floatingInput">Username</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    type="password"
                    className="form-control"
                    id="floatingPassword"
                    name="password"
                    placeHolder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    pattern={"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,8}$"}
                    title={"min 6, max 8, 1 uppercase, 1 lowercase, 1 number"}
                    required
                  />
                  <label htmlFor="floatingPassword">Password</label>
                </div>
                <div className="d-grid">
                  <button
                    className="btn btn-warning btn-login text-uppercase fw-bold text-white"
                    type="submit"
                  >
                    Register
                  </button>
                </div>
                <div class="form-group col-lg-12 mx-auto d-flex align-items-center my-4">
                  <div class="border-bottom w-100 ml-5"></div>
                  <span class="px-2 small text-muted font-weight-bold text-muted">
                    OR
                  </span>
                  <div class="border-bottom w-100 mr-5"></div>
                </div>
                <div class="text-center w-100">
                  <p class="text-muted font-weight-bold">
                    Already Registered?
                    <Link to="/login" className="text-primary ml-2">
                      Login
                    </Link>
                  </p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
