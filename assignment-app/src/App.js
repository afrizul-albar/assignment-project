import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import Login from'./pages/Login/Login';
import Register from './pages/Register/Register';
import Page404 from'./pages/404/Page404';
import Home from'./pages/Home/Home.jsx';
import { useSelector } from 'react-redux';

const loading = (
  <div className="d-flex justify-content-center">
    <div className="spinner-border" role="status">
      <span className="visually-hidden">Loading...</span>
    </div>
  </div>
)

function App() {
  const token = useSelector((state) => state.token);
  
  const CustomRoute = ({...props}) => {
    return (
      token ? <Redirect to="/" /> : <Route {...props} />
    )
  }

  return (
    <BrowserRouter>
        <Switch>
          <CustomRoute path="/login" name="Login Page" render={props => <Login {...props} />} />
          <CustomRoute path="/register" name="Register Page" render={props => <Register {...props} />} />
          <Route path="/404" name="404 Page" render={props => <Page404 {...props} />} />
          <Route path ="/" name="Default" render={props => <Home {...props} />} />
        </Switch>
    </BrowserRouter>
  );
}

export default App;
