import React from "react";

export default function TableCategory() {
  return (
    <div className="table-responsive-sm">
      <table class="table">
        <thead className="table-dark">
          <tr>
            <th>#</th>
            <th>ID</th>
            <th>Category</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
