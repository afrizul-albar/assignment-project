import React from "react";

export default function TableRoom() {
  return (
    <div className="table-responsive-sm">
      <table class="table text-center">
        <thead className="table-dark">
          <tr>
            <th>#</th>
            <th>ID</th>
            <th>Room</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>Otto</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
