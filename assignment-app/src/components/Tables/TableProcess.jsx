import React, { useEffect } from "react";

export default function TableProcess(props) {
  const { data, onUpdate, target } = props;

  return (
    <div className="table-responsive-sm">
      <table class="table">
        <thead className="table-dark">
          <tr>
            <th>ID</th>
            <th>Process</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => {
            return (
              <tr key={index}>
                <td>{item.id}</td>
                <td>{item.process}</td>
                <td>
                  <button className="btn btn-primary" data-bs-toggle="modal" data-bs-target={target}
                    onClick={onUpdate(item)}>
                    Edit
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
