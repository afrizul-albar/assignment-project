import React from "react";

export default function TableAssignee() {
  return (
    <div className="table-responsive-sm">
      <table class="table">
        <thead className="table-dark">
          <tr>
            <th>#</th>
            <th>ID</th>
            <th>Task</th>
            <th>Assignee</th>
            <th>Role</th>
            <th>Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Process</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>Mark</td>
            <td>Mark</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
