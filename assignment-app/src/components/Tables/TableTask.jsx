import React from "react";

export default function TableTask() {
  return (
    <div className="table-responsive-sm">
      <table class="table">
        <thead className="table-dark">
          <tr>
            <th>#</th>
            <th>ID</th>
            <th>Task</th>
            <th>Category</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Room</th>
            <th>PIC</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>Mark</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
