import React from "react";

export default function TableRole() {
  return (
    <div className="table-responsive-sm">
      <table class="table text-center">
        <thead className="table-dark">
          <tr>
            <th>#</th>
            <th>ID</th>
            <th>Task Role</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
