import React from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { authAction } from "../../redux/reducer/AuthReducer";
import profile from '../../asserts/profile.jpg'
import { BASE_URL_IMAGE } from "../../config";

export default function Sidebar() {
  const role = useSelector((state) => state.role);
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();

  const sideBar = () => {

    let data = linkUser;
    if (role === "ROLE_ADMIN") data = admin;
    console.log(data);
    return data;
  };

  return (
    <div className="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-warning bg-gradient" >
      <div className="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
        <Link
          to="/"
          className="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-white text-decoration-none"
        >
          <span className="fs-3 d-none d-sm-inline">NexSign</span>
        </Link>
        <ul
          className="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start"
          id="menu"
        >
          {sideBar().map((item, index) => {
            return (
              item.link ?
              <>
                <li key={index} className="nav-item">
                  <Link to={item.link} className="nav-link px-0 d-flex align-items-center text-white">
                    <i className={`fs-4 ${item.icon}`}></i>
                    <span className="ms-1 d-none d-sm-inline text-white">{item.name}</span>
                  </Link>
                </li>
              </>
              :
              <>
                <li key={index}>
                  <Link
                    role="button"
                    data-bs-toggle="collapse"
                    aria-expanded="false"
                    data-bs-target={`#${item.target}`}
                    aria-controls={item.target}
                    className="nav-link px-0 d-flex align-items-center text-white"
                  >
                    <i className={`fs-4 ${item.icon}`}></i>
                    <span className="ms-1 d-none d-sm-inline text-white">{item.name}</span>
                  </Link>
                  <ul
                      className="collapse nav flex-column ms-2"
                      id={item.target}
                      data-bs-parent={item.name}
                    >
                    {
                      item.subMenu.map((submenu, index) => {
                        return(
                        <li className="w-100" key={index}>
                          <Link to={submenu.link} className="nav-link px-0 d-flex align-items-center text-white">
                            <i className={`fs-5 ${submenu.icon}`}></i>
                            <span className="ms-1 d-none d-sm-inline text-white">{submenu.name}</span>
                          </Link>
                        </li>
                        )
                      })
                    }
                    </ul>
                </li>
              </>
            );
          })}
        </ul>
        <hr />
        <div className="dropdown pb-4">
          <a
            href="#"
            className="d-flex align-items-center text-white text-decoration-none dropdown-toggle"
            id="dropdownUser1"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            <img
              src={profile}
              width="30"
              height="30"
              className="rounded-circle"
            />
            <span className="d-none d-sm-inline mx-1">{user.username}</span>
          </a>
          <ul className="dropdown-menu dropdown-menu-dark text-small shadow">
            <li>
              <Link className="dropdown-item" to="/profile">
                Profile
              </Link>
            </li>
            <li>
              <hr className="dropdown-divider" />
            </li>
            <li>
              <a className="dropdown-item" onClick={() => dispatch(authAction.logout())}>
                Sign out
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export const admin = [
  {
    name: "Home",
    icon: "bi bi-house-fill",
    link: "/home",
  },
  {
    name: "Data",
    icon: "bi bi-pie-chart-fill",
    target: "subMenuData",
    subMenu: [
      {
        name: "Employee",
        icon: "bi bi-people-fill",
        link: "/data/employee",
      },
      {
        name: "Process",
        icon: "bi bi-arrow-repeat",
        link: "/data/process",
      },
      {
        name: "Room",
        icon: "bi bi-door-closed-fill",
        link: "/data/room",
      },
      {
        name: "Task Role",
        icon: "bi bi-circle-square",
        link: "/data/role",
      },
      {
        name: "Category",
        icon: "bi bi-tags-fill",
        link: "/data/category",
      },
      {
        name: "Task",
        icon: "bi bi-list-task",
        link: "/data/taskList",
      },
      {
        name: "Assigned",
        icon: "bi bi-person-lines-fill",
        link: "/data/taskAssigned",
      },
    ],
  },
  {
    name: "Approval",
    icon: "bi bi-check2-square",
    target: "subMenuApproval",
    subMenu: [
      {
        name: "Employee",
        icon: "bi bi-person-check-fill",
        link: "/approval/employee",
      },
      {
        name: "Assignee",
        icon: "bi bi-ui-checks",
        link: "/approval/taskAssignee",
      },
    ],
  },
  {
    name: "Report",
    icon: "bi bi-file-earmark-bar-graph-fill",
    link: "/report",
  },
];

export const linkUser = [
  {
    name: "Home",
    icon: "bi bi-house-fill",
    link: "/home",
  },
  {
    name: "Assign",
    icon: "bi bi-check2-square",
    link: "/assign",
  },
  {
    name: "Request",
    icon: "bi bi-list-task",
    link: "/request",
  },
  {
    name: "History",
    icon: "bi bi-clock-history",
    link: "/history",
  }
];