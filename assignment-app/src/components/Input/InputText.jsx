import React from 'react';

export default function InputText(props) {
    const {
        type,
        className,
        name,
        value,
        onChange,
        placeHolder,
        disabled,
        id,
        required
    } = props;

    return (
        <input 
        type={type} 
        className={className}
        id={id}
        name={name}
        value={value}
        onChange={onChange}
        placeholder={placeHolder}
        disabled={disabled}
        required={required}
        />
    )
}