import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import profile from "../../../asserts/profile.jpg";
import { BASE_URL_IMAGE } from "../../../config";
import { authAction } from "../../../redux/reducer/AuthReducer";
import { updateData } from "../../../service/Fetch";

export default function Profile() {
  const [id, setId] = useState();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [position, setPosition] = useState("");
  const [avatar, setAvatar] = useState("");
  const [res, setRes] = useState({ success: true, data: null });

  const token = useSelector((state) => state.token);
  const user = useSelector((state) => state.user);
  const dispatch =  useDispatch();

  useEffect(() => {
    console.log(user);
    setId(user.id);
    setName(user.name);
    setEmail(user.email);
    setUsername(user.username);
    setPosition(user.position);
    setAvatar(user.avatar);
  }, [res]);

  const updateProfileHandler = (e) => {
    e.preventDefault();
    console.log("aaaa");

    const form = {
      name: name,
      email: email,
      username: username,
    };

    swal({
      title: "Are you sure?",
      text: "Are you sure to update your profile?",
      icon: "info",
      buttons: true,
    }).then((willDelete) => {
      if (willDelete) {
        updateData(`employee/updateEmployee/${id}`, form, token)
          .then((response) => {
            const isUsernameChange = user.username != username ? true : false;
            dispatch(authAction.updateProfile(response.data));
            setRes({ success: true, data: response.data });
            swal("Success update profile!", {
              icon: "success",
            });

            if (isUsernameChange) {
              swal("You have to login again!", {
                icon: "info",
              });
              dispatch(authAction.logout());
            }
          })
          .catch(err => {
            swal(err.response.data, {
              icon: "error",
            });
          })
      }
    });
  };

  return (
    <div className="container rounded bg-white">
      <div className="row">
        <h3>Profile Page</h3>
        <hr />
        <div className="col-md-12 border-right">
          <div className="d-flex flex-column align-items-center text-center">
            <img
              className="rounded-circle"
              width="150px"
              src={profile}
            />
          </div>
        </div>
        <div className="col-md-12 border-right">
          <form onSubmit={updateProfileHandler}>
            <div className="row mt-2">
              <div className="col-md-6 my-2">
                <label className="labels">Name</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  required
                />
              </div>
              <div className="col-md-6 my-2">
                <label className="labels">Email</label>
                <input
                  type="email"
                  className="form-control"
                  value={email}
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </div>
              <div className="col-md-6 my-2">
                <label className="labels">Username</label>
                <input
                  type="text"
                  className="form-control"
                  value={username}
                  placeholder="Username"
                  onChange={(e) => setUsername(e.target.value)}
                  required
                />
              </div>
              <div className="col-md-6 my-2">
                <label className="labels">Position</label>
                <input
                  type="text"
                  className="form-control"
                  value={position}
                  placeholder="Position"
                  disabled
                />
              </div>
            </div>
            <div className="mt-5">
              <input type="submit" className="btn btn-primary profile-button" value="Save Profile" />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
