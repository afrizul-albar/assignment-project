import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Calendar, momentLocalizer, Views } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { getData } from "../../../../service/Fetch";
import moment from "moment";

export default function Main() {
  const [events, setEvents] = useState([]);
  const localizer = momentLocalizer(moment);

  const user = useSelector(state => state.user)
  const token = useSelector(state => state.token)
  const history = useHistory();

  useEffect(() => {
    getData(`taskAssignee/findAllSchedule/${user.id}`, token)
    .then(response => {
        const data = response.data
        let dataEvents = [];
        console.log(data);

        if (data.length > 0) {

            data.map(item => {
                const event = {
                    id : item.id,
                    title : item.task.taskName,
                    start: `${item.date} ${item.startTime}`,
                    end: `${item.date} ${item.endTime}`
                }
                dataEvents.push(event);
            })
            setEvents(dataEvents)
        }
    })
  }, [])

  const eventHandler = (event) => {
    history.push({
        pathname: "/detail/assignee",
        search: `action=READ=${event.id}`,
        state: {},
      });
  }

  return (
    <div className="container">
      <div className="row">
        <h3>Home Page</h3>
        <hr />
      </div>
      <div className="row d-flex">
        <div style={{ height: 580 }}>
          <Calendar
            popup
            events={events}
            localizer={localizer}
            step={60}
            startAccessor="start"
            endAccessor="end"
            onSelectEvent={(event) => eventHandler(event)}
            views={["month", "agenda"]}
            defaultDate={new Date()}
          />
        </div>
      </div>
    </div>
  );
}
