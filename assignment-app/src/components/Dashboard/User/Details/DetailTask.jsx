import moment from "moment";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Redirect, useHistory } from "react-router";
import swal from "sweetalert";
import { getData, updateData } from "../../../../service/Fetch";

export default function DetailTask() {
  const [dataProcess, setDataProcess] = useState([]);
  const [dataTask, setDataTask] = useState(null);
  const [dataAssignee, setDataAssignee] = useState(null);
  const [id, setId] = useState();
  const [date, setDate] = useState("");
  const [startTime, setStartTime] = useState("");
  const [endTime, setEndTime] = useState("");
  const [process, setProcess] = useState();
  const [isApprove, setIsApprove] = useState("");
  const [employee, setEmployee] = useState("");
  const [processNow, setProcessNow] = useState("");
  const token = useSelector((state) => state.token);

  const history = useHistory();
  const query = history.location.search;
  const queryArr = query.split("=");
  const action = queryArr[1];
  const idTask = queryArr[2];

  useEffect(() => {
    if (action === "UPDATE" || action === "READ") {
      if (idTask) {
      getData(`taskAssignee/findById/${idTask}`, token)
        .then((res) => {
          const data = res.data;
          console.log(data);
          setId(data.id);
          setDataTask(data.task);
          setDataAssignee(data);
          setDate(data.date);
          setStartTime(data.startTime);
          setEndTime(data.endTime);
          setProcess(data.taskProcess.id);
          setIsApprove(data.isApprove);
          setEmployee(data.assignee);
          setProcessNow(data.taskProcess.process);
        })
        .catch((err) => {
          console.log(err);
          swal({icon: "error", title: "Error!", text: err.response.data})
          history.push("/home");
        });
      } else {
        swal({icon: "error", title: "Error!", text: "ID not found"})
        history.push("/home");
      }
    }
  }, []);

  useEffect(() => {
    getData("process/findAll", token)
      .then((res) => {
        setDataProcess(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const updateHandler = (e) => {
    e.preventDefault();

    const form = {
      date: date,
      startTime: startTime,
      endTime: endTime,
      processId: process,
    };

    updateData(`taskAssignee/user/updateTaskAssigned/${id}`, form, token)
      .then((res) => {
        swal({
          title: "Success!",
          text: res.data,
          icon: "success",
        });
        history.push("/home");
      })
      .catch((err) => {
        swal({
          title: "Error!",
          text: err.response.data,
          icon: "error",
        });
      });
  };

  const dateFormatter = (date) => {
    return moment(date).format("YYYY-MM-DD");
  };

  const form = () => {
    return (
      <form className="row" onSubmit={updateHandler}>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputTaskName" className="form-label">
            Task
          </label>
          <input
            type="text"
            className="form-control"
            id="inputTaskName"
            value={dataTask ? dataTask.taskName : ""}
            disabled
          />
        </div>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputCategory" className="form-label">
            Category
          </label>
          <input
            type="text"
            className="form-control"
            id="inputCategory"
            value={dataTask ? dataTask.taskCategory.taskCategory : ""}
            disabled
          />
        </div>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputRoom" className="form-label">
            Room
          </label>
          <input
            type="text"
            className="form-control"
            id="inputRoom"
            value={dataTask ? dataTask.room.room : ""}
            disabled
          />
        </div>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputPIC" className="form-label">
            People In Charge
          </label>
          <input
            type="text"
            className="form-control"
            id="inputPIC"
            value={dataTask ? dataTask.pic.name : ""}
            disabled
          />
        </div>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputRole" className="form-label">
            Role
          </label>
          <input
            type="text"
            className="form-control"
            id="inputRole"
            value={dataAssignee ? dataAssignee.taskRole.taskRole : ""}
            disabled
          />
        </div>
        <div className="col-sm-2 my-1">
          <label htmlFor="inputStartDate" className="form-label">
            Date
          </label>
          <input
            type="date"
            className="form-control"
            id="inputStartDate"
            min={dataTask ? dataTask.startDate : ""}
            max={dataTask ? dataTask.endDate : ""}
            value={date}
            onChange={(e) => setDate(e.target.value)}
            disabled={
              action !== "UPDATE" || processNow !== "Pre-Process" ? true : false
            }
          />
        </div>
        <div className="col-sm-2 my-1">
          <label htmlFor="inputStartTime" className="form-label">
            Start Time
          </label>
          <input
            type="time"
            className="form-control"
            id="inputStartTime"
            value={startTime}
            onChange={(e) => setStartTime(e.target.value)}
            disabled={
              action !== "UPDATE" || processNow !== "Pre-Process" ? true : false
            }
          />
        </div>
        <div className="col-sm-2 my-1">
          <label htmlFor="inputStartTime" className="form-label">
            End Time
          </label>
          <input
            type="time"
            className="form-control"
            id="inputEndTime"
            value={endTime}
            onChange={(e) => setEndTime(e.target.value)}
            disabled={
              action !== "UPDATE" || processNow !== "Pre-Process" ? true : false
            }
          />
        </div>
        <div class="col-sm-4 my-1">
          <label htmlFor="inputProcess" class="form-label">
            Process
          </label>
          <select
            id="inputProcess"
            className="form-select"
            value={process}
            onChange={(e) => setProcess(e.target.value)}
            disabled={
              isApprove == "Pending" ||
              employee == null ||
              processNow == "Complete" ||
              action == "READ"
                ? true
                : false
            }
          >
            <option value={null} className="text-muted">
              Choose Process...
            </option>
            <hr className="dropdown-divider" />
            {dataProcess.map((item, idx) => {
              return <option value={item.id}>{item.process}</option>;
            })}
          </select>
        </div>
        <div className="col-sm-12 my-4">
          {action === "UPDATE" ? (
            <input
              type="submit"
              className="btn btn-primary"
              value="Edit"
              disabled={dateFormatter(Date.now()) > date || processNow === "Complete" ? true : false}
            />
          ) : (
            <div>
              <button
                className="btn btn-primary"
                disabled={dateFormatter(Date.now()) > date || processNow === "Complete" ? true : false}
                onClick={() =>
                  history.push({
                    pathname: "/detail",
                    search: `action=UPDATE=${id}`,
                  })
                }
              >
                Change to Edit
              </button>
            </div>
          )}
        </div>
      </form>
    );
  };

  return (
    <div className="container">
      <div className="row">
        <h3>Detail Page</h3>
        <hr />
      </div>
      <div>
        {idTask && (action === "UPDATE" || action === "READ") ? (
          form()
        ) : (
          <Redirect to="/404" />
        )}
      </div>
    </div>
  );
}
