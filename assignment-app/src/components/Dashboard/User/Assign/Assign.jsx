import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import swal from 'sweetalert';
import { getData, updateData } from '../../../../service/Fetch';

export default function Assign() {
    const [dataAssignee, setDataAssignee] = useState([]);
  const [res, setRes] = useState({ success: true, data: null });
  const [page, setPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [kwd, setKwd] = useState("");
  const token = useSelector((state) => state.token);
  const user = useSelector((state) => state.user);

  const history = useHistory();

  useEffect(() => {
    getData(`taskAssignee/findAllTask?pageNo=${page}&keyword=${keyword}`, token)
      .then((response) => {
        const data = response.data;
        setTotalPage(data.totalPages);
        setDataAssignee(data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [res, page, keyword]);

  const handlerAssign = (idTask, e) => {
    e.preventDefault();

    swal({
      title: "Are you sure?",
      text: "Are you sure you want to take on this task?",
      icon: "info",
      buttons: true,
    }).then((ok) => {
      if (ok) {
        updateData(
          `taskAssignee/user/assignEmployee/${idTask}/${user.id}`,
          null,
          token
        )
          .then((response) => {
            setRes({ success: true, data: response.data });
            swal(response.data, {
              icon: "success",
            });
          })
          .catch((err) => {
            console.log(err);
            swal(err.response.data, {
              icon: "error",
            });
          });
      }
    });
  };

  const onHandler = (id) => {
    history.push({
      pathname: "/detail",
      search: `action=READ=${id}`,
    });
  };

  const onSearch = () => {
    console.log("AAAAAAAAAAAAAAA");
    setPage(0);
    setKeyword(kwd);
  };

  const reset = () => {
    setKwd("");
    setKeyword("");
    setPage(0);
  };

  return (
    <div className="container">
      <div className="row">
        <h3>Home Page</h3>
        <hr />
      </div>
      <div className="row d-flex justify-content-end">
        <div className="col-md-6 my-1">
          <div class="input-group mb-3">
            <input
              type="text"
              class="form-control"
              placeholder="Search..."
              value={kwd}
              onChange={(e) => setKwd(e.target.value)}
            />
            <button
              class="btn btn-outline-warning"
              type="button"
              id="button-addon2"
              onClick={onSearch}
            >
              Search
            </button>
            {keyword.length > 0 && (
              <>
                <button
                  type="button"
                  class="btn btn-outline-warning dropdown-toggle dropdown-toggle-split"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-end">
                  <li>
                    <span class="dropdown-item" onClick={reset}>
                      Reset
                    </span>
                  </li>
                </ul>
              </>
            )}
          </div>
        </div>
      </div>
      <div className="row d-flex flex-column my-3">
        <div className="table-responsive-sm">
          <table className="table table-striped table-hover text-center">
            <thead className="table-light">
              <tr>
                <th>ID</th>
                <th>Task</th>
                <th>Date</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {dataAssignee.length > 0 ? (
                dataAssignee.map((item, idx) => {
                  return (
                    <tr key={idx}>
                      <td>{item.id}</td>
                      <td>{item.task.taskName}</td>
                      <td>{item.date}</td>
                      <td>{item.startTime}</td>
                      <td>{item.endTime}</td>
                      <td>
                        <button
                          className="btn btn-success mx-1"
                          onClick={() => onHandler(item.id)}
                        >
                          Detail
                        </button>
                        <button
                          className="btn btn-warning mx-1"
                          onClick={(e) => handlerAssign(item.id, e)}
                        >
                          Assign
                        </button>
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td colSpan="6">Today Assignee still empty</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        {dataAssignee.length > 0 && (
          <div className="d-flex justify-content-center">
            <nav aria-label="pagination">
              <ul className="pagination">
                <li className={page === 0 ? "page-item disabled" : "page-item"}>
                  <span
                    onClick={() => setPage(page - 1)}
                    className="page-link"
                    style={{ cursor: "pointer" }}
                  >
                    Previous
                  </span>
                </li>
                {Array.from(Array(totalPage), (e, i) => {
                  return (
                    <li
                      key={i}
                      className={page === i ? "page-item active" : "page-item"}
                      style={{ cursor: "pointer" }}
                    >
                      <span
                        onClick={() => setPage(i)}
                        className="page-link"
                        tyle={{ cursor: "pointer" }}
                      >
                        {i + 1}
                      </span>
                    </li>
                  );
                })}
                <li
                  className={
                    page + 1 === totalPage ? "page-item disabled" : "page-item"
                  }
                >
                  <span
                    onClick={() => setPage(page + 1)}
                    className="page-link"
                    style={{ cursor: "pointer" }}
                  >
                    Next
                  </span>
                </li>
              </ul>
            </nav>
          </div>
        )}
      </div>
    </div>
  );
}