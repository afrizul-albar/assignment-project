import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import { getData, postData, updateData } from "../../../../service/Fetch";
import ModalDelete from "../../../Modals/delete/ModalDelete";
import ModalAddProcess from "../../../Modals/save/Process/ModalAddProcess";
import ModalEditProcess from "../../../Modals/save/Process/ModalEditProcess";

export default function Process() {
  const [data, setData] = useState([]);
  const [resp, setResp] = useState({ success: false, data: null });
  const [id, setId] = useState();
  const [process, setProcess] = useState("");
  const [page, setPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [kwd, setKwd] = useState("");
  const token = useSelector((state) => state.token);

  useEffect(() => {
    const url = `process/admin/findAllPagination?pageNo=${page}&keyword=${keyword}`;
    getData(url, token)
      .then((response) => {
        const data = response.data;
        setData(data.content);
        setTotalPage(data.totalPages);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [resp, page, keyword]);

  const HandlerCreate = (e) => {
    e.preventDefault();
    document.getElementById("closeAddProcess").click();

    let form = {
      process: process,
    };

    postData("process/admin/createTaskProcess", form, token)
      .then((response) => {
        setResp({ success: true, data: response.data });
        swal({ icon: "success", title: "Success!", text: "Success add process!" });
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: "Failed add process!" });
        console.log(err);
      });
    setProcess("");
  };

  const HandlerUpdate = (e) => {
    e.preventDefault();
    document.getElementById("closeEditProcess").click();

    let form = {
      process: process,
    };

    updateData(`process/admin/updateTaskProcess/${id}`, form, token)
      .then((response) => {
        setResp({ success: true, data: response.data });
        swal({ icon: "success", title: "Success!", text: "Success update process!" });
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: err.response.data });
      });
    setProcess("");
    setId();
  };

  const HandlerDelete = (e) => {
    e.preventDefault();
    document.getElementById("buttonCloseDelete").click();

    updateData(`process/admin/deleteTaskProcess/${id}`, null, token)
      .then((response) => {
        setResp({ success: true, data: response.data });
        setPage(0);
        swal({ icon: "success", title: "Success!", text: "Success delete process" });
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: err.response.data });
      });
  };

  const onAdd = () => {
    setProcess("");
  };

  const onUpdate = (object) => {
    setId(object.id);
    setProcess(object.process);
  };

  const onDelete = (id) => {
    setId(id);
  };

  const onSearch = () => {
    setPage(0);
    setKeyword(kwd);
  };

  const reset = () => {
    setKwd("");
    setKeyword("");
    setPage(0);
  };

  return (
    <div className="container">
      <div className="row">
        <h3>Task Process Page</h3>
        <hr />
      </div>
      <div className="row d-flex justify-content-end">
        <div className="col-md-6 my-1">
          <div class="input-group mb-3">
            <input
              type="text"
              class="form-control"
              placeholder="Search..."
              value={kwd}
              onChange={(e) => setKwd(e.target.value)}
            />
            <button
              class="btn btn-outline-warning"
              type="button"
              id="button-addon2"
              onClick={onSearch}
            >
              Search
            </button>
            {keyword.length > 0 && (
              <>
                <button
                  type="button"
                  class="btn btn-outline-warning dropdown-toggle dropdown-toggle-split"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-end">
                  <li>
                    <span class="dropdown-item" onClick={reset}>
                      Reset
                    </span>
                  </li>
                </ul>
              </>
            )}
          </div>
        </div>
      </div>
      <div className="row d-flex flex-column justify-content-center my-3">
        {data.length > 0 ? (
          <>
            <div className="table-responsive-sm">
              <table class="table table-striped table-hover text-center">
                <thead className="table-light">
                  <tr>
                    <th>ID</th>
                    <th>Process</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td>{item.id}</td>
                        <td>{item.process}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div className="d-flex justify-content-center">
              <nav aria-label="pagination">
                <ul className="pagination">
                  <li
                    className={page === 0 ? "page-item disabled" : "page-item"}
                  >
                    <span
                      onClick={() => setPage(page - 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Previous
                    </span>
                  </li>
                  {Array.from(Array(totalPage), (e, i) => {
                    return (
                      <li
                        key={i}
                        className={
                          page === i ? "page-item active" : "page-item"
                        }
                        style={{ cursor: "pointer" }}
                      >
                        <span
                          onClick={() => setPage(i)}
                          className="page-link"
                          tyle={{ cursor: "pointer" }}
                        >
                          {i + 1}
                        </span>
                      </li>
                    );
                  })}
                  <li
                    className={
                      page + 1 === totalPage
                        ? "page-item disabled"
                        : "page-item"
                    }
                  >
                    <span
                      onClick={() => setPage(page + 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Next
                    </span>
                  </li>
                </ul>
              </nav>
            </div>
          </>
        ) : (
          <h4 className="text-center">Data is still empty</h4>
        )}
      </div>

      {/* Modal Add Process */}
      <ModalAddProcess
        handlerCreate={HandlerCreate}
        process={process}
        onChangeProcess={(e) => setProcess(e.target.value)}
      />

      {/* Modal Edit Process */}
      <ModalEditProcess
        handlerUpdate={HandlerUpdate}
        process={process}
        id={id}
        onChangeProcess={(e) => setProcess(e.target.value)}
      />

      {/* Modal Delete Process */}
      <ModalDelete handlerDelete={HandlerDelete} />
    </div>
  );
}
