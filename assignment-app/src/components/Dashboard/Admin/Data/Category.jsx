import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import { getData, postData, updateData } from "../../../../service/Fetch";
import ModalDelete from "../../../Modals/delete/ModalDelete";
import ModalAddCategory from "../../../Modals/save/Category/ModalAddCategory";
import ModalEditCategory from "../../../Modals/save/Category/ModalEditCategory";

export default function Category() {
  const [data, setData] = useState([]);
  const [resp, setResp] = useState({ success: false, data: null });
  const [id, setId] = useState("");
  const [category, setCategory] = useState("");
  const [page, setPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [kwd, setKwd] = useState("");
  const token = useSelector((state) => state.token);

  useEffect(() => {
    const url = `category/findAllPagination?pageNo=${page}&keyword=${keyword}`;
    getData(url, token)
      .then((response) => {
        const data = response.data;
        setData(data.content);
        setTotalPage(data.totalPages);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [resp, page, keyword]);

  const HandlerCreate = (e) => {
    e.preventDefault();
    document.getElementById("closeAddCategory").click();

    let form = {
      taskCategory: category,
    };

    postData("category/createTaskCategory", form, token)
      .then((response) => {
        setResp({ success: true, data: response.data });
        swal({ icon: "success", title: "Success!", text: "Success add task category!" });
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: "Failed add task category!" });
      });
  };

  const HandlerUpdate = (e) => {
    e.preventDefault();
    document.getElementById("closeEditCategory").click();

    let form = {
      taskCategory: category,
    };

    updateData(`category/updateTaskCategory/${id}`, form, token)
      .then((response) => {
        setResp({ success: true, data: response.data });
        swal({ icon: "success", title: "Success!", text: "Success update task category!" });
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: err.response.data });
      });
  };

  const HandlerDelete = (e) => {
    e.preventDefault();
    document.getElementById("buttonCloseDelete").click();

    updateData(`category/deleteTaskCategory/${id}`, null, token)
      .then((response) => {
        setResp({ success: true, data: response.data });
        setPage(0);
        swal({ icon: "success", title: "Success!", text: "Success delete category" });
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: err.response.data });
      });
  };

  const onAdd = () => {
    setCategory("");
  };

  const onUpdate = (object) => {
    console.log(object);
    setId(object.id);
    setCategory(object.taskCategory);
  };

  const onDelete = (id) => {
    setId(id);
  };

  const onSearch = () => {
    console.log("AAAAAAAAAAAAAAA");
    setPage(0);
    setKeyword(kwd);
  };

  const reset = () => {
    setKwd("");
    setKeyword("");
    setPage(0);
  };

  return (
    <div className="container">
      <div className="row">
        <h3>Task Category Page</h3>
        <hr />
      </div>
      <div className="row d-flex">
        <div className="col-md-6 my-1">
          <button
            type="button"
            className="btn btn-warning text-white"
            data-bs-toggle="modal"
            data-bs-target="#addCategory"
            onClick={onAdd}
          >
            <i className="bi bi-plus"></i> Add Category
          </button>
        </div>
        <div className="col-md-6 my-1">
          <div class="input-group mb-3">
            <input
              type="text"
              class="form-control"
              placeholder="Search..."
              value={kwd}
              onChange={(e) => setKwd(e.target.value)}
            />
            <button
              class="btn btn-outline-warning"
              type="button"
              id="button-addon2"
              onClick={onSearch}
            >
              Search
            </button>
            {keyword.length > 0 && (
              <>
                <button
                  type="button"
                  class="btn btn-outline-warning dropdown-toggle dropdown-toggle-split"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-end">
                  <li>
                    <span class="dropdown-item" onClick={reset}>
                      Reset
                    </span>
                  </li>
                </ul>
              </>
            )}
          </div>
        </div>
      </div>
      <div className="row d-flex flex-column my-3">
        {data.length > 0 ? (
          <>
            <div className="table-responsive-sm">
              <table class="table table-striped table-hover text-center">
                <thead className="table-light">
                  <tr>
                    <th>ID</th>
                    <th>Category</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((item, idx) => {
                    return (
                      <tr key={idx}>
                        <td>{item.id}</td>
                        <td>{item.taskCategory}</td>
                        <td>
                          <button
                            className="btn btn-primary mx-1"
                            data-bs-toggle="modal"
                            data-bs-target="#updateCategory"
                            onClick={() => onUpdate(item)}
                          >
                            <i className="bi bi-pencil-square"></i>
                          </button>
                          <button
                            className="btn btn-danger mx-1"
                            data-bs-toggle="modal"
                            data-bs-target="#modalDelete"
                            onClick={() => onDelete(item.id)}
                          >
                            <i className="bi bi-trash"></i>
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div className="d-flex justify-content-center">
              <nav aria-label="pagination">
                <ul className="pagination">
                  <li
                    className={page === 0 ? "page-item disabled" : "page-item"}
                  >
                    <span
                      onClick={() => setPage(page - 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Previous
                    </span>
                  </li>
                  {Array.from(Array(totalPage), (e, i) => {
                    return (
                      <li
                        key={i}
                        className={
                          page === i ? "page-item active" : "page-item"
                        }
                        style={{ cursor: "pointer" }}
                      >
                        <span
                          onClick={() => setPage(i)}
                          className="page-link"
                          tyle={{ cursor: "pointer" }}
                        >
                          {i + 1}
                        </span>
                      </li>
                    );
                  })}
                  <li
                    className={
                      page + 1 === totalPage
                        ? "page-item disabled"
                        : "page-item"
                    }
                  >
                    <span
                      onClick={() => setPage(page + 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Next
                    </span>
                  </li>
                </ul>
              </nav>
            </div>
          </>
        ) : (
          <h4 className="text-center">Data is still empty</h4>
        )}
      </div>

      {/* Modal Add Category */}
      <ModalAddCategory
        category={category}
        onChangeCategory={(e) => setCategory(e.target.value)}
        handlerCreate={HandlerCreate}
      />

      {/* Modal Edit Category */}
      <ModalEditCategory
        id={id}
        category={category}
        onChangeCategory={(e) => setCategory(e.target.value)}
        handlerUpdate={HandlerUpdate}
      />

      <ModalDelete handlerDelete={HandlerDelete} />
    </div>
  );
}
