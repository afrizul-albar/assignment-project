import moment from "moment";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import swal from "sweetalert";
import { getData, updateData } from "../../../../service/Fetch";
import ModalDelete from "../../../Modals/delete/ModalDelete";
import TableAssignee from "../../../Tables/TableAssignee";

export default function TaskAssignee() {
  const [id, setId] = useState();
  const [data, setData] = useState([]);
  const [res, setRes] = useState({ success: true, data: null });
  const [page, setPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [kwd, setKwd] = useState("");

  const history = useHistory();
  const token = useSelector((state) => state.token);

  useEffect(() => {
    const url = `taskAssignee/admin/findAllPagination/Approved?pageNo=${page}&keyword=${keyword}`;
    getData(url, token)
      .then((response) => {
        const data = response.data;
        setData(data.content);
        setTotalPage(data.totalPages);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [res, page, keyword]);

  const addHandler = () => {
    history.push({
      pathname: "/detail/assignee",
      search: "action=CREATE",
      state: {},
    });
  };

  const HandlerDelete = (e) => {
    e.preventDefault();
    document.getElementById("buttonCloseDelete").click();

    updateData(`taskAssignee/admin/deleteTaskAssigned/${id}`, null, token)
      .then((response) => {
        setRes({ success: true, data: response.data });
        setPage(0);
        swal({
          icon: "success",
          title: "Success!",
          text: "Success delete task assignee!",
        });
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: err.response.data });
      });
  };

  const onHandler = (action, id) => {
    history.push({
      pathname: "/detail/assignee",
      search: `action=${action}=${id}`,
      state: {},
    });
  };

  const onDelete = (id) => {
    setId(id);
  };

  const onSearch = () => {
    console.log("AAAAAAAAAAAAAAA");
    setPage(0);
    setKeyword(kwd);
  };

  const reset = () => {
    setKwd("");
    setKeyword("");
    setPage(0);
  };

  return (
    <div className="container">
      <div className="row">
        <h3>Task Assignee Page</h3>
        <hr />
      </div>
      <div className="row d-flex">
        <div className="col-md-6 my-1">
          <button
            type="button"
            className="btn btn-warning text-white"
            onClick={addHandler}
          >
            <i className="bi bi-plus"></i> Add Task Assignee
          </button>
        </div>
        <div className="col-md-6 my-1">
          <div class="input-group mb-3">
            <input
              type="text"
              class="form-control"
              placeholder="Search..."
              value={kwd}
              onChange={(e) => setKwd(e.target.value)}
            />
            <button
              class="btn btn-outline-warning"
              type="button"
              id="button-addon2"
              onClick={onSearch}
            >
              Search
            </button>
            {keyword.length > 0 && (
              <>
                <button
                  type="button"
                  class="btn btn-outline-warning dropdown-toggle dropdown-toggle-split"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-end">
                  <li>
                    <span class="dropdown-item" onClick={reset}>
                      Reset
                    </span>
                  </li>
                </ul>
              </>
            )}
          </div>
        </div>
      </div>
      <div className="row d-flex flex-column my-3">
        {data.length > 0 ? (
          <>
            <div className="table-responsive-sm">
              <table class="table text-center">
                <thead className="table-light">
                  <tr>
                    <th>ID</th>
                    <th>Task</th>
                    <th>Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((item, idx) => {
                    return (
                      <tr key={idx}>
                        <td>{item.id}</td>
                        <td>{item.task.taskName}</td>
                        <td>{item.date}</td>
                        <td>{item.startTime}</td>
                        <td>{item.endTime}</td>
                        <td className="row m-0 justify-content-center">
                          <button
                            className="btn btn-success mx-1 col-md-3"
                            onClick={() => onHandler("READ", item.id)}
                          >
                            <i className="bi bi-card-list"></i>
                          </button>
                          <button
                            className="btn btn-primary mx-1 col-md-2"
                            onClick={() => onHandler("UPDATE", item.id)}
                            disabled={
                              item.taskProcess.process === "Complete" ||
                              moment(Date.now()).format("YYYY-MM-DD") >
                                item.date
                                ? true
                                : false
                            }
                          >
                            <i className="bi bi-pencil-square"></i>
                          </button>
                          <button
                            className="btn btn-danger mx-1 col-md-3"
                            data-bs-toggle="modal"
                            data-bs-target="#modalDelete"
                            onClick={() => onDelete(item.id)}
                          >
                            <i className="bi bi-trash"></i>
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div className="d-flex justify-content-center">
              <nav aria-label="pagination">
                <ul className="pagination">
                  <li
                    className={page === 0 ? "page-item disabled" : "page-item"}
                  >
                    <span
                      onClick={() => setPage(page - 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Previous
                    </span>
                  </li>
                  {Array.from(Array(totalPage), (e, i) => {
                    return (
                      <li
                        key={i}
                        className={
                          page === i ? "page-item active" : "page-item"
                        }
                        style={{ cursor: "pointer" }}
                      >
                        <span
                          onClick={() => setPage(i)}
                          className="page-link"
                          tyle={{ cursor: "pointer" }}
                        >
                          {i + 1}
                        </span>
                      </li>
                    );
                  })}
                  <li
                    className={
                      page + 1 === totalPage
                        ? "page-item disabled"
                        : "page-item"
                    }
                  >
                    <span
                      onClick={() => setPage(page + 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Next
                    </span>
                  </li>
                </ul>
              </nav>
            </div>
          </>
        ) : (
          <h4 className="text-center">Data is still empty</h4>
        )}
      </div>
      {/* Delete Modal */}
      <ModalDelete handlerDelete={HandlerDelete} />
    </div>
  );
}
