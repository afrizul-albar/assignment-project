import moment from "moment";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import swal from "sweetalert";
import { getData, updateData } from "../../../../service/Fetch";
import ModalDelete from "../../../Modals/delete/ModalDelete";
import TableTask from "../../../Tables/TableTask";

export default function TaskList() {
  const [id, setId] = useState();
  const [taskList, setTaskList] = useState([]);
  const [res, setRes] = useState({ success: true, data: null });
  const [page, setPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [kwd, setKwd] = useState("");

  const history = useHistory();
  const token = useSelector((state) => state.token);

  useEffect(() => {
    const url = `taskList/findAllPagination?pageNo=${page}&keyword=${keyword}`;
    getData(url, token)
      .then((response) => {
        const data = response.data;
        setTaskList(data.content);
        setTotalPage(data.totalPages);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [res, page, keyword]);

  const HandlerDelete = (e) => {
    e.preventDefault();
    document.getElementById("buttonCloseDelete").click();

    updateData(`taskList/deleteTaskList/${id}`, null, token)
      .then((response) => {
        setRes({ success: true, data: response.data });
        setPage(0);
        swal({ icon: "success", title: "Success!", text: "Success delete task!" });
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: err.response.data });
      });
  };

  const addHandler = () => {
    history.push({
      pathname: "/detail/task",
      search: "action=CREATE",
      state: {},
    });
  };

  const onHandler = (action, id) => {
    history.push({
      pathname: "/detail/task",
      search: `action=${action}=${id}`,
      state: {},
    });
  };

  const onDelete = (id) => {
    setId(id);
  };

  const onSearch = () => {
    console.log("AAAAAAAAAAAAAAA");
    setPage(0);
    setKeyword(kwd);
  };

  const reset = () => {
    setKwd("");
    setKeyword("");
    setPage(0);
  };

  const dateFormatter = (date) => {
    return moment(date).format("YYYY-MM-DD")
  }

  return (
    <div className="container">
      <div className="row">
        <h3>Task List Page</h3>
        <hr />
      </div>
      <div className="row d-flex">
        <div className="col-md-6 my-1">
          <button
            type="button"
            className="btn btn-warning text-white"
            onClick={addHandler}
          >
            <i className="bi bi-plus"></i> Add Task
          </button>
        </div>
        <div className="col-md-6 my-1">
          <div class="input-group mb-3">
            <input
              type="text"
              class="form-control"
              placeholder="Search..."
              value={kwd}
              onChange={(e) => setKwd(e.target.value)}
            />
            <button
              class="btn btn-outline-warning"
              type="button"
              id="button-addon2"
              onClick={onSearch}
            >
              Search
            </button>
            {keyword.length > 0 && (
              <>
                <button
                  type="button"
                  class="btn btn-outline-warning dropdown-toggle dropdown-toggle-split"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-end">
                  <li>
                    <span class="dropdown-item" onClick={reset}>
                      Reset
                    </span>
                  </li>
                </ul>
              </>
            )}
          </div>
        </div>
      </div>
      <div className="row d-flex flex-column my-3">
        {taskList.length > 0 ? (
          <>
            <div className="table-responsive-sm">
              <table class="table table-striped table-hover text-center">
                <thead className="table-light">
                  <tr>
                    <th>ID</th>
                    <th>Task</th>
                    <th>Category</th>
                    <th>PIC</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {taskList.map((item, idx) => {
                    return (
                      <tr key={idx}>
                        <td>{item.id}</td>
                        <td>{item.taskName}</td>
                        <td>{item.taskCategory.taskCategory}</td>
                        <td>{item.pic.name}</td>
                        <td className="row m-0 justify-content-center">
                          <button
                            className="btn btn-success mx-1 col-md-2"
                            onClick={() => onHandler("READ", item.id)}
                          >
                            <i className="bi bi-card-list"></i>
                          </button>
                          <button
                            className="btn btn-primary mx-1 col-md-2"
                            onClick={() => onHandler("UPDATE", item.id)}
                            disabled={dateFormatter(Date.now()) > item.endDate ? true : false}
                          >
                            <i className="bi bi-pencil-square"></i>
                          </button>
                          <button
                            className="btn btn-danger mx-1 col-md-2"
                            data-bs-toggle="modal"
                            data-bs-target="#modalDelete"
                            onClick={() => onDelete(item.id)}
                          >
                            <i className="bi bi-trash"></i>
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div className="d-flex justify-content-center">
              <nav aria-label="pagination">
                <ul className="pagination">
                  <li
                    className={page === 0 ? "page-item disabled" : "page-item"}
                  >
                    <span
                      onClick={() => setPage(page - 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Previous
                    </span>
                  </li>
                  {Array.from(Array(totalPage), (e, i) => {
                    return (
                      <li
                        key={i}
                        className={
                          page === i ? "page-item active" : "page-item"
                        }
                        style={{ cursor: "pointer" }}
                      >
                        <span
                          onClick={() => setPage(i)}
                          className="page-link"
                          tyle={{ cursor: "pointer" }}
                        >
                          {i + 1}
                        </span>
                      </li>
                    );
                  })}
                  <li
                    className={
                      page + 1 === totalPage
                        ? "page-item disabled"
                        : "page-item"
                    }
                  >
                    <span
                      onClick={() => setPage(page + 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Next
                    </span>
                  </li>
                </ul>
              </nav>
            </div>
          </>
        ) : (
          <h4 className="text-center">Data is still empty</h4>
        )}
      </div>

      {/* Delete Modal */}
      <ModalDelete handlerDelete={HandlerDelete} />
    </div>
  );
}
