import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import swal from "sweetalert";
import { getData } from "../../../../service/Fetch";
import ModalReport from "../../../Modals/filter/ModalReport";
import { CSVLink } from "react-csv";
import moment from "moment";

export default function Report() {
  const [data, setData] = useState([]);
  const [dataCSV, setDataCSV] = useState([]);
  const [page, setPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [keywordTaskName, setKeywordTaskName] = useState("");
  const [kwdTaskName, setKwdTaskName] = useState("");
  const [keywordPIC, setKeywordPIC] = useState("");
  const [kwdPIC, setKwdPIC] = useState("");
  const [keywordAssignee, setKeywordAssignee] = useState("");
  const [kwdAssignee, setKwdAssignee] = useState("");
  const [keywordRoom, setKeywordRoom] = useState("");
  const [kwdRoom, setKwdRoom] = useState("");

  const history = useHistory();
  const token = useSelector((state) => state.token);

  useEffect(() => {
    const url =
      `taskAssignee/admin/findTaskReport?pageNo=${page}&keywordTaskName=${keywordTaskName}&` +
      `keywordPIC=${keywordPIC}&keywordAssignee=${keywordAssignee}&keywordRoom=${keywordRoom}`;
    getData(url, token)
      .then((response) => {
        const data = response.data;
        setData(data.content);
        setTotalPage(data.totalPages);
        console.log(data.content);
      })
      .catch((err) => {});
  }, [page, keywordTaskName, keywordAssignee, keywordPIC, keywordRoom]);

  const handlerFiter = (e) => {
    e.preventDefault();
    document.getElementById("closeModalReport").click();

    setPage(0);
    setKeywordTaskName(kwdTaskName);
    setKeywordAssignee(kwdAssignee);
    setKeywordPIC(kwdPIC);
    setKeywordRoom(kwdRoom);
    setDataCSV([]);
  };

  const onFilter = () => {
    setKwdTaskName(keywordTaskName);
    setKwdAssignee(keywordAssignee);
    setKwdPIC(keywordPIC);
    setKwdRoom(keywordRoom);
  };

  const resetFilter = () => {
    setPage(0);
    setKeywordTaskName("");
    setKeywordAssignee("");
    setKeywordPIC("");
    setKeywordRoom("");
    setDataCSV([]);
  };

  const onHandler = (id) => {
    history.push({
      pathname: "/detail/assignee",
      search: `action=READ=${id}`,
      state: {},
    });
  };

  const headers = [
    { label: "ID Task Assignee", key: "id" },
    { label: "ID Task", key: "idTask" },
    { label: "Task Name", key: "taskName" },
    { label: "ID PIC", key: "idPIC" },
    { label: "PIC", key: "pic" },
    { label: "ID Room", key: "idRoom" },
    { label: "Room", key: "room" },
    { label: "ID Assignee", key: "idAssignee" },
    { label: "Assignee", key: "assignee" },
    { label: "Role", key: "role" },
    { label: "Date", key: "date" },
    { label: "Start Time", key: "startTime" },
    { label: "End Time", key: "endTime" },
    { label: "Process", key: "process" },
  ];

  const fetchReportCSV = () => {
    const url = `taskAssignee/admin/getTaskReportToCSV?keywordTaskName=${keywordTaskName}&` +
    `keywordPIC=${keywordPIC}&keywordAssignee=${keywordAssignee}&keywordRoom=${keywordRoom}`
    getData(url, token)
      .then((response) => {
        const data = response.data;
        let dataCSV = [];
        if (data.length > 0) {
          data.map((item) => {
            const csvField = {
              id: item.id,
              idTask: item.task.id,
              taskName: item.task.taskName,
              idPIC: item.task.pic.id,
              pic: item.task.pic.name,
              idRoom: item.task.room.id,
              room: item.task.room.room,
              idAssignee: item.assignee.id,
              assignee: item.assignee.name,
              role: item.taskRole.taskRole,
              date: item.date,
              startTime: item.startTime,
              endTime: item.endTime,
              process: item.taskProcess.process,
            };
            dataCSV.push(csvField);
          });
          setDataCSV(dataCSV);
        } else {
          swal({
            title: "error!",
            text: "Data Report still empty",
            icon: "error",
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const dateFormatter = (date) => {
    return moment(date).format("YYYY-MM-DD");
  };

  return (
    <div className="container">
      <div className="row">
        <h3>Report Page</h3>
        <hr />
      </div>
      <div className="row d-flex">
        <div className="col-md-10 my-1">
          {dataCSV.length > 0 ? (
            <CSVLink
              data={dataCSV}
              headers={headers}
              filename={`Report-${dateFormatter(Date.now())}.csv`}
              className="btn btn-warning"
            >
              <i className="bi bi-download"></i> Download CSV
            </CSVLink>
          ) : (
            <button
              type="button"
              className="btn btn-warning"
              onClick={fetchReportCSV}
            >
              <i className="bi bi-arrow-left-right"></i> Export Report
            </button>
          )}
        </div>
        <div className="col-md-2 my-1">
          <div className="input-group mb-3">
            <button
              className="btn btn-outline-warning"
              type="button"
              data-bs-toggle="modal"
              data-bs-target="#modalReport"
              onClick={onFilter}
            >
              Filter
            </button>
            {(keywordTaskName.length > 0 ||
              keywordAssignee.length > 0 ||
              keywordPIC.length > 0 ||
              keywordRoom.length > 0) && (
              <>
                <button
                  type="button"
                  class="btn btn-outline-warning dropdown-toggle dropdown-toggle-split"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-end">
                  <li>
                    <span className="dropdown-item" onClick={resetFilter}>
                      Reset
                    </span>
                  </li>
                </ul>
              </>
            )}
          </div>
        </div>
      </div>
      <div className="row d-flex flex-column my-3">
        {data.length > 0 ? (
          <>
            <div className="table-responsive-sm">
              <table class="table text-center">
                <thead className="table-light">
                  <tr>
                    <th>ID</th>
                    <th>Task</th>
                    <th>Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((item, idx) => {
                    return (
                      <tr key={idx}>
                        <td>{item.id}</td>
                        <td>{item.task.taskName}</td>
                        <td>{item.date}</td>
                        <td>{item.startTime}</td>
                        <td>{item.endTime}</td>
                        <td className="row m-0 justify-content-center">
                          <button
                            className="btn btn-success mx-1 col-md-6"
                            onClick={() => onHandler(item.id)}
                          >
                            <i className="bi bi-card-list"></i>
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div className="d-flex justify-content-center">
              <nav aria-label="pagination">
                <ul className="pagination">
                  <li
                    className={page === 0 ? "page-item disabled" : "page-item"}
                  >
                    <span
                      onClick={() => setPage(page - 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Previous
                    </span>
                  </li>
                  {Array.from(Array(totalPage), (e, i) => {
                    return (
                      <li
                        key={i}
                        className={
                          page === i ? "page-item active" : "page-item"
                        }
                        style={{ cursor: "pointer" }}
                      >
                        <span
                          onClick={() => setPage(i)}
                          className="page-link"
                          tyle={{ cursor: "pointer" }}
                        >
                          {i + 1}
                        </span>
                      </li>
                    );
                  })}
                  <li
                    className={
                      page + 1 === totalPage
                        ? "page-item disabled"
                        : "page-item"
                    }
                  >
                    <span
                      onClick={() => setPage(page + 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Next
                    </span>
                  </li>
                </ul>
              </nav>
            </div>
          </>
        ) : (
          <h4 className="text-center">Data is still empty</h4>
        )}
      </div>
      {/* Modal Filter */}
      <ModalReport
        kwdTaskName={kwdTaskName}
        kwdAssignee={kwdAssignee}
        kwdPIC={kwdPIC}
        kwdRoom={kwdRoom}
        onChangeTaskName={(e) => setKwdTaskName(e.target.value)}
        onChangeAssignee={(e) => setKwdAssignee(e.target.value)}
        onChangePIC={(e) => setKwdPIC(e.target.value)}
        onChangeRoom={(e) => setKwdRoom(e.target.value)}
        handlerFilter={handlerFiter}
      />
    </div>
  );
}
