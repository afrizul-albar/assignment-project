import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Redirect, useHistory } from "react-router";
import { getData, postData, updateData } from "../../../../service/Fetch";
import profile from "../../../../asserts/profile.jpg";
import { BASE_URL_IMAGE } from "../../../../config";
import swal from "sweetalert";

export default function DetailEmployee() {
  const [id, setId] = useState();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [reTypePassword, setReTypePassword] = useState("");
  const [position, setPosition] = useState("");
  const [avatar, setAvatar] = useState("");
  const token = useSelector((state) => state.token);

  const history = useHistory();
  const query = history.location.search;
  const queryArr = query.split("=");
  const action = queryArr[1];
  const idEmployee = queryArr[2];

  useEffect(() => {
    if (action === "UPDATE" || action === "READ") {
      if (idEmployee) {
      getData(`employee/findById/${idEmployee}`, token)
        .then((response) => {
          const data = response.data;
          console.log(data);
          setId(data.id);
          setName(data.name);
          setEmail(data.email);
          setUsername(data.username);
          setPosition(data.position);
          setAvatar(data.avatar);
        })
        .catch((err) => {
          swal({ icon: "error", title: "Error!", text: err.response.data });
          history.push("/data/employee");
        })
      } else {
        swal({ icon: "error", title: "Error!", text: "ID Not Found!" });
        history.push("/data/employee");
      }
    }
  }, []);

  const createHandler = (e) => {
    e.preventDefault();

    const form = {
      name: name,
      email: email,
      username: username,
      password: password,
      position: position
    };

    postData("employee/admin/register", form, token)
      .then((response) => {
        swal({
          title: "Success!",
          text: response.data,
          icon: "success"
        });
        history.push("/data/employee");
      })
      .catch((err) => {
        swal({
          title: "Error!",
          text: err.response.data,
          icon: "error",
        });
      });
  };

  const updateHandler = (e) => {
    e.preventDefault();

    const form = {
      name: name,
      email: email,
      username: username,
      position: position,
    };

    console.log(form);

    updateData(`employee/admin/updateEmployee/${id}`, form, token)
      .then((response) => {
        swal({
          title: "Success!",
          text: response.data,
          icon: "success",
        });
        history.push("/data/employee");
      })
      .catch((err) => {
        swal({
          title: "Error!",
          text: err.response.data,
          icon: "error",
        });
        console.log(err, err.response.data);
      });
  };

  const formAdd = () => {
    return (
      <form className="row" onSubmit={createHandler}>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputName" className="form-label">
            Name
          </label>
          <input
            type="text"
            className="form-control"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputEmail" className="form-label">
            Email
          </label>
          <input
            type="email"
            className="form-control"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputEmail" className="form-label">
            Position
          </label>
          <select
            id="inputPosition"
            className="form-select"
            value={position}
            onChange={(e) => setPosition(e.target.value)}
            required
          >
            <option value={""} className="text-muted">
              Choose Position...
            </option>
            <hr className="dropdown-divider" />
            <option value="Leader">Leader</option>
            <option value="Employee">Employee</option>
          </select>
        </div>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputUsername" className="form-label">
            Username
          </label>
          <input
            type="text"
            className="form-control"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputEmail" className="form-label">
            Password
          </label>
          <input
            type="password"
            className="form-control"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            pattern={"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,8}$"}
            title={"min 6, max 8, 1 uppercase, 1 lowercase, 1 number"}
            required
          />
        </div>
        <div className="col-sm-6 my-4">
          <input type="submit" className="btn btn-success" value="Create" />
        </div>
      </form>
    );
  };

  const formReadAndEdit = () => {
    return (
      <form className="row" onSubmit={updateHandler}>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputName" className="form-label">
            Name
          </label>
          <input
            type="text"
            className="form-control"
            value={name}
            onChange={(e) => setName(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputEmail" className="form-label">
            Email
          </label>
          <input
            type="email"
            className="form-control"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputEmail" className="form-label">
            Position
          </label>
          <select
            id="inputPosition"
            className="form-select"
            value={position}
            onChange={(e) => setPosition(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          >
            <option value="Leader">Leader</option>
            <option value="Employee">Employee</option>
          </select>
        </div>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputUsername" className="form-label">
            Username
          </label>
          <input
            type="text"
            className="form-control"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          />
        </div>
        <div className="col-sm-12 my-4">
          {action === "UPDATE" ? (
            <input type="submit" className="btn btn-primary" value="Edit" />
          ) : (
            <button
              className="btn btn-primary"
              onClick={() =>
                history.push({
                  pathname: "/detail/employee",
                  search: `action=UPDATE=${id}`,
                })
              }
            >
              Change to Edit
            </button>
          )}
        </div>
      </form>
    );
  };

  return (
    <div className="container">
      <div className="row">
        <h3>Detail Page</h3>
        <hr />
      </div>
      <div>
        {action === "CREATE" ? (
          formAdd()
        ) : action === "UPDATE" || action === "READ" ? (
          formReadAndEdit()
        ) : (
          <Redirect to="/404" />
        )}
      </div>
    </div>
  );
}
