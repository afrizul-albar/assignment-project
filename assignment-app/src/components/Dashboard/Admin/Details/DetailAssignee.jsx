import moment from "moment";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Redirect, useHistory } from "react-router";
import swal from "sweetalert";
import { getData, postData, updateData } from "../../../../service/Fetch";

export default function DetailAssignee() {
  const [dataEmployee, setDataEmployee] = useState([]);
  const [dataTask, setDataTask] = useState([]);
  const [dataProcess, setDataProcess] = useState([]);
  const [dataRole, setDataRole] = useState([]);
  const [id, setId] = useState();
  const [task, setTask] = useState();
  const [assignee, setAssignee] = useState();
  const [role, setRole] = useState();
  const [date, setDate] = useState("");
  const [startTime, setStartTime] = useState("");
  const [endTime, setEndTime] = useState("");
  const [process, setProcess] = useState();
  const [minDate, setMinDate] = useState("");
  const [maxDate, setMaxDate] = useState("");
  const [oldProcess, setOldProcess] = useState("");
  const [oldDate, setOldDate] = useState("");
  const token = useSelector((state) => state.token);

  const history = useHistory();
  const query = history.location.search;
  const queryArr = query.split("=");
  const action = queryArr[1];
  const idTask = queryArr[2];

  useEffect(() => {
    if (action === "UPDATE" || action === "READ") {
      if (idTask) {
        getData(`taskAssignee/findById/${idTask}`, token)
          .then((response) => {
            const datas = response.data;
            console.log(datas);
            setId(datas.id);
            setTask(datas.task.id);
            if (datas.assignee) {
              setAssignee(datas.assignee.id);
            }
            setRole(datas.taskRole.id);
            setDate(datas.date);
            setStartTime(datas.startTime);
            setEndTime(datas.endTime);
            setProcess(datas.taskProcess.id);
            setMinDate(datas.task.startDate);
            setMaxDate(datas.task.endDate);
            setOldProcess(datas.taskProcess.process);
            setOldDate(datas.date);
          })
          .catch((err) => {
            swal({ icon: "error", title: "Error!", text: err.response.data });
            history.push("/data/taskAssigned");
          });
      } else {
        swal({ icon: "error", title: "Error!", text: "ID not found" });
        history.push("/data/taskAssigned");
      }
    }
  }, []);

  useEffect(() => {
    if (action !== "READ") {
      getData("employee/admin/findAllApproved", token)
      .then((res) => {
        setDataEmployee(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    } else {
      getData("employee/admin/findAllNoCondition", token)
      .then((res) => {
        setDataEmployee(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    }
  }, []);

  useEffect(() => {
    if (action != "READ") {
      getData("taskList/findAllForForm", token)
        .then((res) => {
          setDataTask(res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      getData("taskList/findAllNoCondition", token)
        .then((res) => {
          setDataTask(res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, []);

  useEffect(() => {
    getData("process/findAll", token)
      .then((res) => {
        setDataProcess(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    if (action !== "READ") {
      getData("role/findAll", token)
      .then((res) => {
        setDataRole(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    } else {
      getData("role/findAllNoCondition", token)
      .then((res) => {
        setDataRole(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    }
  }, []);

  useEffect(() => {
    if (typeof task === "string") {
      console.log("objectAAAAAAAAAAAAA");
      const data = dataTask.find(({ id }) => id == task);
      setMinDate(data.startDate);
      setMaxDate(data.endDate);
    }
  }, [task]);

  const createHandler = (e) => {
    e.preventDefault();

    const form = {
      taskId: task,
      assigneeId: assignee,
      roleId: role,
      date: date,
      startTime: startTime,
      endTime: endTime,
    };

    postData("taskAssignee/admin/createTaskAssigned", form, token)
      .then((res) => {
        swal({ icon: "success", title: "Success!", text: "Success add task assignee!" });
        history.push("/data/taskAssigned");
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: err.response.data });
      });
  };

  const updateHandler = (e) => {
    e.preventDefault();

    const form = {
      taskId: task,
      assigneeId: assignee,
      roleId: role,
      date: date,
      startTime: startTime,
      endTime: endTime,
      processId: process,
    };

    updateData(`taskAssignee/admin/updateTaskAssigned/${id}`, form, token)
      .then((res) => {
        swal({ icon: "success", title: "Success!", text: "Success update task assignee!" });
        history.push("/data/taskAssigned");
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: err.response.data });
      });
  };

  const formAdd = () => {
    return (
      <form className="row" onSubmit={createHandler}>
        <div class="col-sm-6 my-1">
          <label htmlFor="inputTask" class="form-label">
            Task
          </label>
          <select
            id="inputTask"
            className="form-select"
            onChange={(e) => setTask(e.target.value)}
            required
          >
            <option value={""} className="text-muted" disabled selected>
              Choose Task...
            </option>
            <hr className="dropdown-divider" />
            {dataTask.map((item, idx) => {
              return <option value={item.id}>{item.taskName}</option>;
            })}
          </select>
        </div>
        <div class="col-sm-6 my-1">
          <label htmlFor="inputRole" class="form-label">
            Role
          </label>
          <select
            id="inputRole"
            className="form-select"
            value={role}
            onChange={(e) => setRole(e.target.value)}
            required
          >
            <option value={""} className="text-muted">
              Choose Role...
            </option>
            <hr className="dropdown-divider" />
            {dataRole.map((item, idx) => {
              return <option value={item.id}>{item.taskRole}</option>;
            })}
          </select>
        </div>
        <div class="col-sm-7 my-1">
          <label htmlFor="inputAssignee" class="form-label">
            Assignee
          </label>
          <select
            id="inputAssignee"
            className="form-select"
            value={assignee}
            onChange={(e) => setAssignee(e.target.value)}
          >
            <option value={""} className="text-muted">
              Choose Assignee...
            </option>
            <hr className="dropdown-divider" />
            {dataEmployee.map((item, idx) => {
              return (
                 <option value={item.id}>{item.name}</option>
              );
            })}
          </select>
        </div>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputStartDate" className="form-label">
            Date
          </label>
          <input
            type="date"
            className="form-control"
            id="inputStartDate"
            min={minDate}
            max={maxDate}
            value={date}
            onChange={(e) => setDate(e.target.value)}
            disabled={task ? false : true}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputStartTime" className="form-label">
            Start Time
          </label>
          <input
            type="time"
            className="form-control"
            id="inputStartTime"
            value={startTime}
            onChange={(e) => setStartTime(e.target.value)}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputStartTime" className="form-label">
            End Time
          </label>
          <input
            type="time"
            className="form-control"
            id="inputEndTime"
            value={endTime}
            onChange={(e) => setEndTime(e.target.value)}
            required
          />
        </div>
        <div className="col-sm-12 my-4">
          <input type="submit" className="btn btn-success" value="Create" />
        </div>
      </form>
    );
  };

  const formReadAndEdit = () => {
    return (
      <form className="row" onSubmit={updateHandler}>
        <div class="col-sm-6 my-1">
          <label htmlFor="inputTask" class="form-label">
            Task
          </label>
          <select
            id="inputTask"
            className="form-select"
            onChange={(e) => setTask(e.target.value)}
            value={task}
            disabled={action === "READ" ? true : false}
            required
          >
            <option value={""} className="text-muted">
              Choose Task...
            </option>
            <hr className="dropdown-divider" />
            {dataTask.map((item, idx) => {
              return <option value={item.id}>{item.taskName}</option>;
            })}
          </select>
        </div>
        <div class="col-sm-6 my-1">
          <label htmlFor="inputRole" class="form-label">
            Role
          </label>
          <select
            id="inputRole"
            className="form-select"
            value={role}
            onChange={(e) => setRole(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          >
            <option value={""} className="text-muted">
              Choose Role...
            </option>
            <hr className="dropdown-divider" />
            {dataRole.map((item, idx) => {
              return <option value={item.id}>{item.taskRole}</option>;
            })}
          </select>
        </div>
        <div class="col-sm-7 my-1">
          <label htmlFor="inputAssignee" class="form-label">
            Assignee
          </label>
          <select
            id="inputAssignee"
            className="form-select"
            value={assignee}
            onChange={(e) => setAssignee(e.target.value)}
            disabled={action === "READ" ? true : false}
          >
            <option value={""} className="text-muted">
              Choose Assignee...
            </option>
            <hr className="dropdown-divider" />
            {dataEmployee.map((item, idx) => {
              return (
                <option value={item.id}>{item.name}</option>
              );
            })}
          </select>
        </div>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputStartDate" className="form-label">
            Date
          </label>
          <input
            type="date"
            className="form-control"
            id="inputStartDate"
            min={minDate}
            max={maxDate}
            value={date}
            onChange={(e) => setDate(e.target.value)}
            disabled={task === null ? true : action === "READ" ? true : false}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputStartTime" className="form-label">
            Start Time
          </label>
          <input
            type="time"
            className="form-control"
            id="inputStartTime"
            value={startTime}
            onChange={(e) => setStartTime(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputStartTime" className="form-label">
            End Time
          </label>
          <input
            type="time"
            className="form-control"
            id="inputEndTime"
            value={endTime}
            onChange={(e) => setEndTime(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          />
        </div>
        <div class="col-sm-4 my-1">
          <label htmlFor="inputProcess" class="form-label">
            Process
          </label>
          <select
            id="inputProcess"
            className="form-select"
            value={process}
            onChange={(e) => setProcess(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          >
            <option value={""} className="text-muted">
              Choose Process...
            </option>
            <hr className="dropdown-divider" />
            {dataProcess.map((item, idx) => {
              return <option value={item.id}>{item.process}</option>;
            })}
          </select>
        </div>
        <div className="col-sm-12 my-4">
          {action === "UPDATE" ? (
            <input type="submit" className="btn btn-primary" value="Edit" 
            disabled=
              {oldProcess === "Complete" || moment(Date.now()).format("YYYY-MM-DD") > oldDate 
              ? true 
              : false
              }
            />
          ) : (
            <button
              className="btn btn-primary"
              onClick={() =>
                history.push({
                  pathname: "/detail/assignee",
                  search: `action=UPDATE=${id}`,
                })
              }
              disabled=
              {oldProcess === "Complete" || moment(Date.now()).format("YYYY-MM-DD") > oldDate 
              ? true 
              : false
              }
            >
              Change to Edit
            </button>
          )}
        </div>
      </form>
    );
  };

  return (
    <div className="container">
      <div className="row">
        <h3>Detail Page</h3>
        <hr />
      </div>
      <div>
        {action === "CREATE" ? (
          formAdd()
        ) : action === "UPDATE" || action === "READ" ? (
          formReadAndEdit()
        ) : (
          <Redirect to="/404" />
        )}
      </div>
    </div>
  );
}
