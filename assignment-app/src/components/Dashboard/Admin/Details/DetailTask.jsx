import React, { useEffect, useState } from "react";
import { Redirect, useHistory } from "react-router";
import moment from "moment";
import { getData, postData, updateData } from "../../../../service/Fetch";
import { useSelector } from "react-redux";
import swal from "sweetalert";

export default function DetailTask() {
  const [dataEmployee, setDataEmployee] = useState([]);
  const [dataRoom, setDataRoom] = useState([]);
  const [dataCategory, setDataCategory] = useState([]);
  const [minDate, setMinDate] = useState();
  const [id, setId] = useState();
  const [taskName, setTaskName] = useState("");
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [pic, setPic] = useState();
  const [category, setCategory] = useState();
  const [room, setRoom] = useState();
  const [oldEndDate, setOldEndDate] = useState();
  const token = useSelector((state) => state.token);

  const history = useHistory();
  const query = history.location.search;
  const queryArr = query.split("=");
  const action = queryArr[1];
  const idTask = queryArr[2];

  useEffect(() => {
    if (action === "UPDATE" || action === "READ") {
      if (idTask) {
        getData(`taskList/findById/${idTask}`, token)
          .then((response) => {
            const data = response.data;
            console.log(data);
            setId(data.id);
            setTaskName(data.taskName);
            setStartDate(data.startDate);
            setEndDate(data.endDate);
            setRoom(data.room.id);
            setCategory(data.taskCategory.id);
            setPic(data.pic.id);
            setMinDate(data.startDate);
            setOldEndDate(data.endDate);
          })
          .catch((err) => {
            swal({ icon: "error", title: "Error!", text: err.response.data });
            history.push("/data/taskList");
          });
      } else {
        swal({ icon: "error", title: "Error!", text: "ID not found" });
        history.push("/data/taskList");
      }
    }
  }, []);

  useEffect(() => {
    if (action !== "READ") {
      getData("employee/admin/findAllLeader", token)
      .then((res) => {
        setDataEmployee(res.data);
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    } else {
      getData("employee/admin/findAllNoCondition", token)
      .then((res) => {
        setDataEmployee(res.data);
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    }
  }, []);

  useEffect(() => {
    if (action !== "READ") {
      getData("room/findAll", token)
      .then((res) => {
        setDataRoom(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    } else {
      getData("room/findAllNoCondition", token)
      .then((res) => {
        setDataRoom(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    }
  }, []);

  useEffect(() => {
    if (action !== "READ") {
      getData("category/findAll", token)
      .then((res) => {
        setDataCategory(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    } else {
      getData("category/findAllNoCondition", token)
      .then((res) => {
        setDataCategory(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    }
  }, []);

  const createHandler = (e) => {
    e.preventDefault();
    console.log(room, category, pic, "AAAAAAAAAAAAA");

    const form = {
      taskName: taskName,
      startDate: startDate,
      endDate: endDate,
      roomId: room,
      categoryId: category,
      picId: pic,
    };

    console.log(form);

    postData("taskList/createTaskList", form, token)
      .then((res) => {
        swal({ icon: "success", title: "Success!", text: res.data });
        history.push("/data/taskList");
      })
      .catch((err) => {
        console.log(err.response);
        swal({ icon: "error", title: "Error!", text: err.response.data });
      });
  };

  const updateHandler = (e) => {
    e.preventDefault();
    console.log(room, category, pic);

    const form = {
      taskName: taskName,
      startDate: startDate,
      endDate: endDate,
      roomId: room,
      categoryId: category,
      picId: pic,
    };

    updateData(`taskList/updateTaskList/${id}`, form, token)
      .then((res) => {
        swal({ icon: "success", title: "Success!", text: res.data });
        history.push("/data/taskList");
      })
      .catch((err) => {
        swal({ icon: "error", title: "Error!", text: err.response.data });
      });
  };

  const formAdd = () => {
    return (
      <>
        <form className="row" onSubmit={createHandler}>
          <div className="col-sm-6 my-1">
            <label htmlFor="inputTaskName" className="form-label">
              Task Name
            </label>
            <input
              type="text"
              className="form-control"
              id="inputTaskName"
              value={taskName}
              onChange={(e) => setTaskName(e.target.value)}
              required
            />
          </div>
          <div className="col-sm-3 my-1">
            <label htmlFor="inputStartDate" className="form-label">
              Start Date
            </label>
            <input
              type="date"
              min={moment(Date.now()).format("YYYY-MM-DD")}
              className="form-control"
              id="inputStartDate"
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
              required
            />
          </div>
          <div className="col-sm-3 my-1">
            <label htmlFor="inputEndDate" className="form-label">
              End Date
            </label>
            <input
              type="date"
              min={moment(Date.now()).format("YYYY-MM-DD")}
              className="form-control"
              id="inputEndDate"
              value={endDate}
              onChange={(e) => setEndDate(e.target.value)}
              required
            />
          </div>
          <div className="col-sm-7 my-1">
            <label htmlFor="inputPIC" className="form-label">
              People In Charge
            </label>
            <select
              id="inputPIC"
              className="form-select"
              value={pic}
              onChange={(e) => setPic(e.target.value)}
              required
            >
              <option value="" className="text-muted">
                Choose People In Charge...
              </option>
              <hr className="dropdown-divider" />
              {dataEmployee.map((item, idx) => {
                return <option value={item.id}>{item.name}</option>;
              })}
            </select>
          </div>
          <div class="col-sm-7 my-1">
            <label htmlFor="inputCategory" class="form-label">
              Category
            </label>
            <select
              id="inputCategory"
              className="form-select"
              value={category}
              onChange={(e) => setCategory(e.target.value)}
              required
            >
              <option value="" className="text-muted">
                Choose Category...
              </option>
              <hr className="dropdown-divider" />
              {dataCategory.map((item, idx) => {
                return <option value={item.id}>{item.taskCategory}</option>;
              })}
            </select>
          </div>
          <div class="col-sm-7 my-1">
            <label htmlFor="inputCategory" class="form-label">
              Room
            </label>
            <select
              id="inputCategory"
              className="form-select"
              value={room}
              onChange={(e) => setRoom(e.target.value)}
              required
            >
              <option value="" className="text-muted">
                Choose Room...
              </option>
              <hr className="dropdown-divider" />
              {dataRoom.map((item, idx) => {
                return <option value={item.id}>{item.room}</option>;
              })}
            </select>
          </div>
          <div className="col-sm-6 my-4">
            <input type="submit" className="btn btn-success" value="Create" />
          </div>
        </form>
      </>
    );
  };

  const formReadAndEdit = () => {
    return (
      <form className="row" onSubmit={updateHandler}>
        <div className="col-sm-6 my-1">
          <label htmlFor="inputTaskName" className="form-label">
            Task Name
          </label>
          <input
            type="text"
            className="form-control"
            id="inputTaskName"
            value={taskName}
            onChange={(e) => setTaskName(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputStartDate" className="form-label">
            Start Date
          </label>
          <input
            type="date"
            min={minDate}
            className="form-control"
            id="inputStartDate"
            value={startDate}
            onChange={(e) => setStartDate(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          />
        </div>
        <div className="col-sm-3 my-1">
          <label htmlFor="inputEndDate" className="form-label">
            End Date
          </label>
          <input
            type="date"
            min={moment(Date.now()).format("YYYY-MM-DD")}
            className="form-control"
            id="inputEndDate"
            value={endDate}
            onChange={(e) => setEndDate(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          />
        </div>
        <div className="col-sm-7 my-1">
          <label htmlFor="inputPIC" className="form-label">
            People In Charge
          </label>
          <select
            id="inputPIC"
            className="form-select"
            value={pic}
            onChange={(e) => setPic(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          >
            <option value="" className="text-muted">
              Choose People In Charge...
            </option>
            <hr />
            {dataEmployee.map((item, idx) => {
              return <option value={item.id}>{item.name}</option>;
            })}
          </select>
        </div>
        <div class="col-sm-7 my-1">
          <label htmlFor="inputCategory" class="form-label">
            Category
          </label>
          <select
            id="inputCategory"
            className="form-select"
            value={category}
            onChange={(e) => setCategory(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          >
            <option value="" className="text-muted">
              Choose Category...
            </option>
            <hr />
            {dataCategory.map((item, idx) => {
              return <option value={item.id}>{item.taskCategory}</option>;
            })}
          </select>
        </div>
        <div class="col-sm-7 my-1">
          <label htmlFor="inputCategory" class="form-label">
            Room
          </label>
          <select
            id="inputCategory"
            className="form-select"
            value={room}
            onChange={(e) => setRoom(e.target.value)}
            disabled={action === "READ" ? true : false}
            required
          >
            <option value="" className="text-muted">
              Choose Room...
            </option>
            <hr />
            {dataRoom.map((item, idx) => {
              return <option value={item.id}>{item.room}</option>;
            })}
          </select>
        </div>
        <div className="col-sm-6 my-4">
          {action === "UPDATE" ? (
            <input type="submit" className="btn btn-primary" value="Edit" 
            disabled={
              moment(Date.now()).format("YYYY-MM-DD") > oldEndDate ? true : false
            }
            />
          ) : (
            <button
              className="btn btn-primary"
              onClick={() =>
                history.push({
                  pathname: "/detail/task",
                  search: `action=UPDATE=${id}`,
                })
              }
              disabled={
                moment(Date.now()).format("YYYY-MM-DD") > endDate ? true : false
              }
            >
              Change to Edit
            </button>
          )}
        </div>
      </form>
    );
  };

  return (
    <div className="container">
      <div className="row">
        <h3>Detail Page</h3>
        <hr />
      </div>
      <div>
        {action === "CREATE" ? (
          formAdd()
        ) : action === "UPDATE" || action === "READ" ? (
          formReadAndEdit()
        ) : (
          <Redirect to="/404" />
        )}
      </div>
    </div>
  );
}
