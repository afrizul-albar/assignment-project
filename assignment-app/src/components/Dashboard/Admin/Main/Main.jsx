import moment from "moment";
import React, { useEffect, useState } from "react";
import { Calendar, momentLocalizer, Views } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import { getData } from "../../../../service/Fetch";

export default function Home() {
  const [events, setEvents] = useState([]);
  const localizer = momentLocalizer(moment);

  const token = useSelector(state => state.token)
  const history = useHistory();

  useEffect(() => {
    getData('taskAssignee/admin/findAllSchedule', token)
    .then(response => {
        const data = response.data
        let dataEvents = [];

        if (data.length > 0) {

            data.map(item => {
                const event = {
                    id : item.id,
                    title : item.task.taskName,
                    start: `${item.date} ${item.startTime}`,
                    end: `${item.date} ${item.endTime}`
                }
                dataEvents.push(event);
            })
            setEvents(dataEvents)
        }
    })
  }, [])

  const eventHandler = (event) => {
    history.push({
        pathname: "/detail/assignee",
        search: `action=READ=${event.id}`,
        state: {},
      });
  }

  return (
    <div className="container">
      <div className="row">
        <h3>Home Page</h3>
        <hr />
      </div>
      <div className="row d-flex">
        <div style={{ height: 580 }}>
          <Calendar
            popup
            events={events}
            localizer={localizer}
            step={60}
            startAccessor="start"
            endAccessor="end"
            onSelectEvent={(event) => eventHandler(event)}
            views={["month", "agenda"]}
            defaultDate={new Date()}
          />
        </div>
      </div>
    </div>
  );
}
