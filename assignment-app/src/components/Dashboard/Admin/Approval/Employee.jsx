import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import swal from "sweetalert";
import { getData, updateData } from "../../../../service/Fetch";
import TableEmployee from "../../../Tables/TableEmployee";

export default function Employee() {
  const [id, setId] = useState();
  const [data, setData] = useState([]);
  const [res, setRes] = useState({ success: true, data: null });
  const [page, setPage] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [keyword, setKeyword] = useState("");
  const [kwd, setKwd] = useState("");

  const history = useHistory();
  const token = useSelector((state) => state.token);

  useEffect(() => {
    const url = `employee/admin/findAll/Pending?pageNo=${page}&keyword=${keyword}`;
    getData(url, token)
      .then((response) => {
        const data = response.data;
        setData(data.content);
        setTotalPage(data.totalPages);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [res, page, keyword]);

  const handlerApproval = (id, approval, e) => {
    e.preventDefault();
    const text =
      approval === "Approved"
        ? "Are you sure you want to approved this Employee?"
        : "Once rejected, you will not be able to recover this Employee!";
    const icon = approval === "Approved" ? "info" : "warning";

    swal({
      title: "Are you sure?",
      text: text,
      icon: icon,
      buttons: true,
    }).then((willDelete) => {
      if (willDelete) {
        updateData(
          `employee/admin/approvalEmployee/${id}/${approval}`,
          null,
          token
        )
          .then((response) => {
            setRes({ success: true, data: response.data });
            swal(response.data, {
              icon: "success",
            });
          })
          .catch((err) => {
            console.log(err);
            swal(err.response.data, {
              icon: "error",
            });
          });
      }
    });
  };

  const onHandler = (action, id) => {
    history.push({
      pathname: "/detail/employee",
      search: `action=${action}=${id}`,
      state: {},
    });
  };

  const onSearch = () => {
    setPage(0);
    setKeyword(kwd);
  };

  const reset = () => {
    setKwd("");
    setKeyword("");
    setPage(0);
  };

  return (
    <div className="container">
      <div className="row">
        <h3>Employee Approval Page</h3>
        <hr />
      </div>
      <div className="row d-flex justify-content-end">
        <div className="col-md-6 my-1">
          <div class="input-group mb-3">
            <input
              type="text"
              class="form-control"
              placeholder="Search..."
              value={kwd}
              onChange={(e) => setKwd(e.target.value)}
            />
            <button
              class="btn btn-outline-warning"
              type="button"
              id="button-addon2"
              onClick={onSearch}
            >
              Search
            </button>
            {keyword.length > 0 && (
              <>
                <button
                  type="button"
                  class="btn btn-outline-warning dropdown-toggle dropdown-toggle-split"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-end">
                  <li>
                    <span class="dropdown-item" onClick={reset}>
                      Reset
                    </span>
                  </li>
                </ul>
              </>
            )}
          </div>
        </div>
      </div>
      <div className="row d-flex flex-column my-3">
      {data.length > 0 ? (
          <>
            <div className="table-responsive-sm">
              <table class="table text-center">
                <thead className="table-light">
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Position</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((item, idx) => {
                    return (
                      <tr key={idx}>
                        <td>{item.id}</td>
                        <td>{item.name}</td>
                        <td>{item.email}</td>
                        <td>{item.username}</td>
                        <td>{item.position}</td>
                        <td className="row m-0 justify-content-center">
                          <button
                            className="btn btn-success mx-1 col-md-3"
                            onClick={() => onHandler("READ", item.id)}
                          >
                            <i className="bi bi-card-list"></i>
                          </button>
                          <button
                            className="btn btn-warning mx-1 col-md-3"
                            onClick={(e) => handlerApproval(item.id, "Approved", e)}
                          >
                            <i className="bi bi-check2-square"></i>
                          </button>
                          <button
                            className="btn btn-danger mx-1 col-md-3"
                            onClick={(e) => handlerApproval(item.id, "Rejected", e)}
                          >
                            <i className="bi bi-x-square"></i>
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div className="d-flex justify-content-center">
              <nav aria-label="pagination">
                <ul className="pagination">
                  <li
                    className={page === 0 ? "page-item disabled" : "page-item"}
                  >
                    <span
                      onClick={() => setPage(page - 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Previous
                    </span>
                  </li>
                  {Array.from(Array(totalPage), (e, i) => {
                    return (
                      <li
                        key={i}
                        className={
                          page === i ? "page-item active" : "page-item"
                        }
                        style={{ cursor: "pointer" }}
                      >
                        <span
                          onClick={() => setPage(i)}
                          className="page-link"
                          tyle={{ cursor: "pointer" }}
                        >
                          {i + 1}
                        </span>
                      </li>
                    );
                  })}
                  <li
                    className={
                      page + 1 === totalPage
                        ? "page-item disabled"
                        : "page-item"
                    }
                  >
                    <span
                      onClick={() => setPage(page + 1)}
                      className="page-link"
                      style={{ cursor: "pointer" }}
                    >
                      Next
                    </span>
                  </li>
                </ul>
              </nav>
            </div>
          </>
        ) : (
          <h4 className="text-center">Data is still empty</h4>
        )}
      </div>
    </div>
  );
}
