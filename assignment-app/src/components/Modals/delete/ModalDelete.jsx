import React from "react";

export default function ModalDelete(props) {
    const {handlerDelete} = props;
  return (
    <div
      className="modal fade"
      id="modalDelete"
      tabIndex="-1"
      aria-labelledby="modalDelete"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Delete
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
              Are you sure want to delete this?
          </div>
          <div className="modal-footer">
            <button
              id="buttonCloseDelete"
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
            <button
              type="button"
              className="btn btn-danger"
              onClick={handlerDelete}
            >
                Delete
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
