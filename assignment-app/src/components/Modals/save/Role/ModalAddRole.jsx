import React from 'react';

export default function ModalAddRole(props) {
    const {handlerCreate, role, onChangeRole} = props;

    return (
        <div
        className="modal fade"
        id="addRole"
        tabIndex="-1"
        aria-labelledby="addRole"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Add Role
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form onSubmit={handlerCreate}>
                <div className="mb-3">
                  <label for="exampleInputEmail1" className="form-label">
                    Role Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="role"
                    name="role"
                    value={role}
                    onChange={onChangeRole}
                    required
                  />
                </div>

                <div className="modal-footer">
                  <button
                    id="closeAddRole"
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <input
                    type="submit"
                    className="btn btn-primary"
                    value="Create"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}