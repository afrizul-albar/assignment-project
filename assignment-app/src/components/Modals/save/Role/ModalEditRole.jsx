import React from 'react';

export default function ModalEditRole(props) {
    const {handlerUpdate, role, id, onChangeRole} = props;

    return (
        <div
        className="modal fade"
        id="updateRole"
        tabIndex="-1"
        aria-labelledby="updateRole"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Update Role
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form onSubmit={handlerUpdate}>
                <div className="mb-3">
                  <label for="exampleInputEmail1" className="form-label">
                    ID
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="roleID"
                    name="roleID"
                    value={id}
                    disabled
                  />
                </div>
                <div className="mb-3">
                  <label for="exampleInputEmail1" className="form-label">
                    Role Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="role"
                    name="role"
                    value={role}
                    onChange={onChangeRole}
                    required
                  />
                </div>
                <div className="modal-footer">
                  <button
                    id="closeEditRole"
                    type="button"
                    className="buttonClose btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <input
                    type="submit"
                    className="btn btn-primary"
                    value="Save"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}