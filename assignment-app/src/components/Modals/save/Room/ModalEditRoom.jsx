import React from 'react';

export default function ModalEditRoom(props) {
    const {handlerUpdate, room, id, onChangeRoom} = props;

    return (
        <div
        className="modal fade"
        id="updateRoom"
        tabIndex="-1"
        aria-labelledby="updateRoom"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Update Room
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form onSubmit={handlerUpdate}>
                <div className="mb-3">
                  <label for="exampleInputEmail1" className="form-label">
                    ID
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="roomID"
                    name="roomID"
                    value={id}
                    disabled
                  />
                </div>
                <div className="mb-3">
                  <label for="exampleInputEmail1" className="form-label">
                    Room Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="room"
                    name="room"
                    value={room}
                    onChange={onChangeRoom}
                    required
                  />
                </div>
                <div className="modal-footer">
                  <button
                    id="closeEditRoom"
                    type="button"
                    className="buttonClose btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <input
                    type="submit"
                    className="btn btn-primary"
                    value="Save"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}