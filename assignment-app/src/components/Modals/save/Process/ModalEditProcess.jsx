import React from 'react';

export default function ModalEditProcess(props) {
    const {handlerUpdate, process, id, onChangeProcess} = props;

    return (
        <div
        className="modal fade"
        id="updateProcess"
        tabIndex="-1"
        aria-labelledby="updateProcess"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Update Process
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form onSubmit={handlerUpdate}>
                <div className="mb-3">
                  <label for="exampleInputEmail1" className="form-label">
                    ID
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="processID"
                    name="processID"
                    value={id}
                    disabled
                  />
                </div>
                <div className="mb-3">
                  <label for="exampleInputEmail1" className="form-label">
                    Process Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="process"
                    name="process"
                    value={process}
                    onChange={onChangeProcess}
                    required
                  />
                </div>
                <div className="modal-footer">
                  <button
                    id="closeEditProcess"
                    type="button"
                    className="buttonClose btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <input
                    type="submit"
                    className="btn btn-primary"
                    value="Save"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}