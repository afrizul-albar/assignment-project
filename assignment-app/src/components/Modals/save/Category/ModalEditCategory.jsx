import React from 'react';

export default function ModalEditCategory(props) {
    const {handlerUpdate, category, id, onChangeCategory} = props;

    return (
        <div
        className="modal fade"
        id="updateCategory"
        tabIndex="-1"
        aria-labelledby="updateCategory"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Update Category
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form onSubmit={handlerUpdate}>
                <div className="mb-3">
                  <label htmlFor="exampleInputEmail1" className="form-label">
                    ID
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="categoryID"
                    name="categoryID"
                    value={id}
                    disabled
                  />
                </div>
                <div className="mb-3">
                  <label for="exampleInputEmail1" className="form-label">
                    Category Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="category"
                    name="category"
                    value={category}
                    onChange={onChangeCategory}
                    required
                  />
                </div>
                <div className="modal-footer">
                  <button
                    id="closeEditCategory"
                    type="button"
                    className="buttonClose btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <input
                    type="submit"
                    className="btn btn-primary"
                    value="Save"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}