import React from 'react';

export default function ModalAddCategory(props) {
    const {handlerCreate, category, onChangeCategory} = props;

    return (
        <div
        className="modal fade"
        id="addCategory"
        tabIndex="-1"
        aria-labelledby="addCategory"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Add Category
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form onSubmit={handlerCreate}>
                <div className="mb-3">
                  <label htmlFor="exampleInputEmail1" className="form-label">
                    Category Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="category"
                    name="category"
                    value={category}
                    onChange={onChangeCategory}
                    required
                  />
                </div>

                <div className="modal-footer">
                  <button
                    id="closeAddCategory"
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <input
                    type="submit"
                    className="btn btn-primary"
                    value="Create"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}