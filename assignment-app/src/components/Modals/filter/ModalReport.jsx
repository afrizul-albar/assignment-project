import React from 'react';

export default function ModalReport(props) {
    const {kwdTaskName, kwdAssignee, kwdPIC, kwdRoom, 
           onChangeTaskName, onChangeAssignee, onChangePIC,
           onChangeRoom, handlerFilter} = props;

    return (
        <div
        className="modal fade"
        id="modalReport"
        tabIndex="-1"
        aria-labelledby="modalReport"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Filter Report
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <form onSubmit={handlerFilter}>
                <div className="mb-3">
                  <label htmlFor="taskName" className="form-label">
                    Task Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    name="taskName"
                    value={kwdTaskName}
                    onChange={onChangeTaskName}
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="assignee" className="form-label">
                    Assignee
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    name="assignee"
                    value={kwdAssignee}
                    onChange={onChangeAssignee}
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="pic" className="form-label">
                    People In Charge
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    name="pic"
                    value={kwdPIC}
                    onChange={onChangePIC}
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="room" className="form-label">
                    Room
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    name="room"
                    value={kwdRoom}
                    onChange={onChangeRoom}
                  />
                </div>
                <div className="modal-footer">
                  <button
                    id="closeModalReport"
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                  <input
                    type="submit"
                    className="btn btn-primary"
                    value="Filter"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}