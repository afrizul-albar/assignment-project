import { createSlice } from "@reduxjs/toolkit";

const initialAuthState = {
    token: "",
    role: "",
    user: {}
}

const authSlice = createSlice({
    name: "auth",
    initialState: initialAuthState,
    reducers: {
        login(state, action) {
            state.token = action.payload.token;
            state.role = action.payload.role;
            state.user = action.payload.user;
        },
        updateProfile(state, action) {
            state.user = action.payload;
        },
        logout(state) {
            state.token = "";
            state.role = "";
            state.user = null
        }
    }
})

export const authAction = authSlice.actions;

export default authSlice.reducer;